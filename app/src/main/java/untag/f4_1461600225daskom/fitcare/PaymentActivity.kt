package untag.f4_1461600225daskom.fitcare

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_payment.*
import untag.f4_1461600225daskom.fitcare.ui.history.HistoryActivity
import untag.f4_1461600225daskom.fitcare.utils.formatCurrency
import untag.f4_1461600225daskom.fitcare.utils.localeId

class PaymentActivity : AppCompatActivity() {

    @ExperimentalStdlibApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        setToolbar()

        tv_payment_invoice.text = intent.getStringExtra("invoice")
        tv_payment_name.text = intent.getStringExtra("therapist_name")
        tv_payment_location.text = intent.getStringExtra("location")
        if (intent.getStringExtra("estimation") != null)
            tv_payment_time.text = intent.getStringExtra("estimation").split(" ").map { it.capitalize(localeId) }.joinToString(" ")
        tv_payment_cost.text = "Rp ${intent.getStringExtra("total_cost").toFloat().formatCurrency()}"

        button_to_history.setOnClickListener {
            val intent = Intent(this, HistoryActivity::class.java)
            startActivity(intent)
            finishAffinity()
        }
    }

    private fun setToolbar() {
        toolbar_payment.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar_payment.setNavigationOnClickListener { finish() }
    }
}
