package untag.f4_1461600225daskom.fitcare.ui.history

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_history.view.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.History
import untag.f4_1461600225daskom.fitcare.ui.historydetail.HistoryDetailActivity
import untag.f4_1461600225daskom.fitcare.utils.customFormat

class HistoryAdapter: RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    private val orders: MutableList<History.Order> = mutableListOf()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false))
    }

    override fun getItemCount(): Int = orders.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(orders[position], context)
    }

    fun addAllHistory(orders: List<History.Order>) {
        this.orders.clear()
        this.orders.addAll(orders)
        notifyDataSetChanged()

        this.orders.sortByDescending { it.id }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        fun bindItem(item: History.Order, context: Context) {
            when (item.service_id) {
                "1" -> {
                    itemView.tv_history_service.text = "Baby Spa"
                    Glide.with(context).load(R.drawable.icons_babys_room_white_60px).into(itemView.iv_history_icon)
                }
                "2" -> {
                    itemView.tv_history_service.text = "Mom Spa"
                    Glide.with(context).load(R.drawable.icons_babys_spa_white_60px).into(itemView.iv_history_icon)
                }
                "3" -> {
                    itemView.tv_history_service.text = "Kids Spa"
                    Glide.with(context).load(R.drawable.kids_b).into(itemView.iv_history_icon)

                }
                "4" -> {
                    itemView.tv_history_service.text = "Dad Spa"
                    Glide.with(context).load(R.drawable.dad_b).into(itemView.iv_history_icon)
                }
                else -> {}
            }
            val orderDate = item.time.customFormat("yyyy-MM-dd kk:mm:ss", "EEE, dd MMM, kk:mm")
            itemView.tv_history_date.text = orderDate
            itemView.tv_history_state.text = item.invoice.status

            itemView.setOnClickListener {
                context.startActivity(Intent(context, HistoryDetailActivity::class.java).also {
                    it.putExtra("id", item.id.toString())
                })
            }
        }
    }
}