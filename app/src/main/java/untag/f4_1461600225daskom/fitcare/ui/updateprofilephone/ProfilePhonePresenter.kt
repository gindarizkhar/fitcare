package untag.f4_1461600225daskom.fitcare.ui.updateprofilephone

import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class ProfilePhonePresenter: BaseMvpPresenterImpl<ProfilePhoneContract.View>(), ProfilePhoneContract.Presenter {
    override fun submitPhone(token: String, phoneNew: String) {
        ApiManager.editProfilePhone(token, phoneNew)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showResponse(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }
}