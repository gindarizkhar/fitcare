package untag.f4_1461600225daskom.fitcare.ui.splash

import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class SplashPresenter: BaseMvpPresenterImpl<SplashContract.View>(), SplashContract.Presenter {
    override fun otpRegister(phone: String, otp: Int) {
        ApiManager.otp(phone,otp)
            .doOnError{  }
            .subscribe(
                Action1 { mView?.showResponse(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.closeApp()}
            )
    }
}