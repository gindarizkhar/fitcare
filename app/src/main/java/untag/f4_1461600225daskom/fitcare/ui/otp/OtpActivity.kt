package untag.f4_1461600225daskom.fitcare.ui.otp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_otp.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.OtpResponse
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager
import untag.f4_1461600225daskom.fitcare.ui.dashboard.DashboardActivity
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity

class OtpActivity : BaseMvpActivity<OtpContract.View,OtpContract.Presenter>(),OtpContract.View {
    override var mPresenter:OtpContract.Presenter=OtpPresenter()
    private val prefManager: PreferenceManager by lazy {
        PreferenceManager(
            this
        )
    }

    private lateinit var phone:String
    private lateinit var from: String

    private lateinit var token: String

    override fun showResponseMessage(response: OtpResponse) {
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show()
        prefManager.token = response.access_token
        Log.d("access token", response.access_token)

        val intent = Intent(this, DashboardActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun hideProgress() {
    }

    override fun showResendSuccessResponse() {
        Toast.makeText(this, "Reset Code Success", Toast.LENGTH_SHORT).show()
    }

    override fun showResendFailResponse() {
        Toast.makeText(this, "Reset Code Failure", Toast.LENGTH_SHORT).show()
    }

    override fun showUpdatePhoneResponse() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)
        var time=30000.toLong()


        phone = intent.getStringExtra("phone")
        from = intent.getStringExtra("from")
        token = "Bearer ${prefManager.token.toString()}"

        Log.d("iki metu opo ora? ",phone)
        setVerification()

        object : CountDownTimer(time, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                tv_resend_otp.text = ""+millisUntilFinished / 1000 +" detik "
            }
            override fun onFinish() {
                tv_resend_otp.text = "Kirim Ulang Code"
                tv_resend_otp.setOnClickListener {
                    if (from == "profile") {
                        mPresenter.resendProfileOtp(token, phone)
                    } else {
                        mPresenter.submitPhoneNumber(phone)
                    }
                    time=30000.toLong()
                    this.start()
                }
            }
        }.start()
    }

    private fun setVerification(){
        et_otp1.addTextChangedListener(object : TextWatcher {
           override fun afterTextChanged(s: Editable?) { }
           override fun beforeTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }
           override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { moveToNextOrPrev() }
       })

       et_otp2.addTextChangedListener(object : TextWatcher{
           override fun afterTextChanged(s: Editable?) { }
           override fun beforeTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }
           override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { moveToNextOrPrev() }
       })

       et_otp3.addTextChangedListener(object : TextWatcher{
           override fun afterTextChanged(s: Editable?) { }
           override fun beforeTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }
           override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { moveToNextOrPrev() }
       })

       et_otp4.addTextChangedListener(object : TextWatcher{
           override fun afterTextChanged(s: Editable?) { }
           override fun beforeTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }
           override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
               val isDelete = moveToNextOrPrev()
               if (!isDelete){
                   val inputMethodManager = applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                   inputMethodManager.hideSoftInputFromWindow(et_otp4.windowToken, 0)
                   submitVerifyData()
               }
           }
       })
    }

    private fun moveToNextOrPrev(): Boolean {
        val text = currentFocus as TextView

        return if (text.length() > 0){
            val next = text.focusSearch(View.FOCUS_RIGHT)
            next?.requestFocus()
            false
        }else {
            val next = text.focusSearch(View.FOCUS_LEFT)
            next?.requestFocus()
            true
        }
    }

    private fun submitVerifyData() {
        val verificationCode =
                et_otp1.text.toString() +
                et_otp2.text.toString() +
                et_otp3.text.toString() +
                et_otp4.text.toString()

        if (from == "profile") {
            mPresenter.updateProfilePhone(token, phone, verificationCode.toInt())
        } else {
            prefManager.otp = verificationCode
            prefManager.phone = phone
            mPresenter.otpRegister(phone, verificationCode.toInt())
        }
    }
}
