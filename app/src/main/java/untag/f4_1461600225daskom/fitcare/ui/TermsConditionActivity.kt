package untag.f4_1461600225daskom.fitcare.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.sucho.placepicker.AddressData
import com.sucho.placepicker.Constants
import kotlinx.android.synthetic.main.terms_and_conditions.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.ui.orderconfirmation.OrderConfirmationActivity
import untag.f4_1461600225daskom.fitcare.utils.maps.MapsActivity

class TermsConditionActivity : AppCompatActivity() {

    private var isButton30MinActive = false
    private var isButton60MinActive = false
    private var isButton90MinActive = false

    private var price: Long = 0
    private var menit = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.terms_and_conditions)

        setToolbar()

        waktu1.setOnClickListener {
            isButton30MinActive=true
            isButton60MinActive=false
            isButton90MinActive=false
            refreshPriceButton()
        }

        waktu2.setOnClickListener {
            isButton30MinActive=false
            isButton60MinActive=true
            isButton90MinActive=false
            refreshPriceButton()
        }

        waktu3.setOnClickListener {
            isButton30MinActive=false
            isButton60MinActive=false
            isButton90MinActive=true
            refreshPriceButton()
        }

        checkbox.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                btn_lanjut.background=resources.getDrawable(R.drawable.custom_button)
                btn_lanjut.setTextColor(resources.getColor(R.color.colorWhite))

                btn_lanjut.isEnabled =true

                //ini adalah kode menuju ke peta
                btn_lanjut.setOnClickListener{
                    //cek jika price == 0
                    if (!isi_harga.text.equals("0")){
                        val intent = Intent(this, MapsActivity::class.java)
                        intent.putExtra("menit", menit.toString())
                        intent.putExtra("price", price.toString())
                        intent.putExtra("service_id", getIntent().getIntExtra("service_id", 1))
                        startActivity(intent)
                    }else{
                        Toast.makeText(this,"Isi durasi terlebih dahulu",Toast.LENGTH_LONG).show()
                    }
                }


            } else {
                btn_lanjut.background=resources.getDrawable(R.drawable.custom_button_3)
                btn_lanjut.setTextColor(resources.getColor(R.color.black))
                btn_lanjut.isEnabled = false
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    val addressData = data?.getParcelableExtra<AddressData>(Constants.ADDRESS_INTENT)
                    val address = addressData?.addressList!![0].getAddressLine(0)
                    val btnlanjut = Intent(this, OrderConfirmationActivity::class.java)
                    btnlanjut.putExtra("menit", menit.toString())
                    btnlanjut.putExtra("price", price.toString())
                    btnlanjut.putExtra("service_id", intent.getIntExtra("service_id", 1))
                    btnlanjut.putExtra("latitude", addressData.latitude)
                    btnlanjut.putExtra("longitude", addressData.longitude)
                    btnlanjut.putExtra("address", address)
                    startActivity(btnlanjut)
                } catch (e: Exception) {
//                    Log.e("MainActivity", e.message.toString())
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun setToolbar() {
        when (intent.getIntExtra("service_id", 0)) {
            1 -> toolbar.title = "Baby Spa"
            2 -> toolbar.title = "Mom Spa"
            3 -> toolbar.title = "Kids Spa"
            4 -> toolbar.title = "Dad Spa"
            else -> toolbar.title = "Checkout"
        }
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
    }

    private fun refreshPriceButton() {
        if (isButton30MinActive){
            waktu1.background=resources.getDrawable(R.drawable.custom_button)
            waktu1.setTextColor(resources.getColor(R.color.colorWhite))
            price=50000
            menit=30
            isi_harga.setText(price.toString())
            TextView7.setText(price.toString())
        } else{
            waktu1.background=resources.getDrawable(R.drawable.custom_button_2)
            waktu1.setTextColor(resources.getColor(R.color.blue))
        }

        if (isButton60MinActive){
            waktu2.background=resources.getDrawable(R.drawable.custom_button)
            waktu2.setTextColor(resources.getColor(R.color.colorWhite))
            price=100000
            menit=60
            isi_harga.setText(price.toString())
            TextView7.setText(price.toString())
        } else{
            waktu2.background=resources.getDrawable(R.drawable.custom_button_2)
            waktu2.setTextColor(resources.getColor(R.color.blue))
        }

        if (isButton90MinActive){
            waktu3.background=resources.getDrawable(R.drawable.custom_button)
            waktu3.setTextColor(resources.getColor(R.color.colorWhite))
            price=150000
            menit=90
            isi_harga.setText(price.toString())
            TextView7.setText(price.toString())
        } else{
            waktu3.background=resources.getDrawable(R.drawable.custom_button_2)
            waktu3.setTextColor(resources.getColor(R.color.blue))
        }
    }
}