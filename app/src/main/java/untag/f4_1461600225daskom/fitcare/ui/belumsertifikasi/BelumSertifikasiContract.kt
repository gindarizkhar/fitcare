package untag.f4_1461600225daskom.fitcare.ui.belumsertifikasi

import okhttp3.MultipartBody
import okhttp3.RequestBody
import untag.f4_1461600225daskom.fitcare.model.BelumSertifikasi
import untag.f4_1461600225daskom.fitcare.model.Dashboard
import untag.f4_1461600225daskom.fitcare.model.HistoryDetail
import untag.f4_1461600225daskom.fitcare.model.RegisMitra
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface BelumSertifikasiContract {
    interface View: BaseMvpView {
        fun showResponseMessage(response: BelumSertifikasi.BelumSertifikasiResponse)
        fun showResponseMessageDetail(response: BelumSertifikasi.BelumSertifikasiDetailResponse)

        fun hideProgress()
        fun hideProgressMessage(it: Throwable?)
    }
    interface Presenter: BaseMvpPresenter<View> {
        fun getSertifikasi(authorization: String

        )

        fun getSertifikasiDetail(authorization: String, id: String)
    }
}