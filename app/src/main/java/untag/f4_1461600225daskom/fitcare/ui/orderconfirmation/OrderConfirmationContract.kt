package untag.f4_1461600225daskom.fitcare.ui.orderconfirmation

import untag.f4_1461600225daskom.fitcare.model.Order
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface OrderConfirmationContract {
    interface View: BaseMvpView {
        fun showResponseMessage(response: Order.OrderResponse)

        fun hideProgress()
        fun showResponseErrorMessage(it: Throwable)
    }
    interface Presenter: BaseMvpPresenter<View> {
        fun submitOrder(token: String,
                        serviceId: String,
                        longitude: String,
                        latitude: String,
                        time: String,
                        note: String,
                        duration: String,
                        payment: String)
    }
}