package untag.f4_1461600225daskom.fitcare.ui.otp

import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class OtpPresenter:BaseMvpPresenterImpl<OtpContract.View>(),OtpContract.Presenter {
    override fun otpRegister(phone: String, otp: Int) {
        ApiManager.otp(phone,otp)
            .doOnError{mView?.showMessageOTP()}
            .subscribe(
                Action1 { mView?.showResponseMessage(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }

    override fun submitPhoneNumber(phone: String) {
        ApiManager.login(phone)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showResendSuccessResponse() },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.showResendFailResponse()}
            )
    }

    override fun updateProfilePhone(token: String, phoneNew: String, otp: Int) {
        ApiManager.verifyProfilePhone(token, phoneNew,otp)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showUpdatePhoneResponse() },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }

    override fun resendProfileOtp(token: String, phoneNew: String) {
        ApiManager.editProfilePhone(token, phoneNew)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showResendSuccessResponse() },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.showResendFailResponse()}
            )
    }
}