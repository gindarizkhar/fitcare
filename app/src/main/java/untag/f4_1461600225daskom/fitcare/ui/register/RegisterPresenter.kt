package untag.f4_1461600225daskom.fitcare.ui.register

import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class RegisterPresenter: BaseMvpPresenterImpl<RegisterContract.View>(),RegisterContract.Presenter {
    override fun submitregister(email: String, phone: String, full_name: String) {
        ApiManager.register(email,full_name,phone)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showResponseMessage(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }
}