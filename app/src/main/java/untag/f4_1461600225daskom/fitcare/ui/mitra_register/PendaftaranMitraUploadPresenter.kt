package untag.f4_1461600225daskom.fitcare.ui.mitra_register

import okhttp3.MultipartBody
import okhttp3.RequestBody
import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class PendaftaranMitraUploadPresenter: BaseMvpPresenterImpl<PendaftaranMitraUploadContract.View>(), PendaftaranMitraUploadContract.Presenter {

    override fun subimitRegisMitra(
        token: String,
        service_id: RequestBody,
        full_name: RequestBody,
        address: RequestBody,
        phone: RequestBody,
        email: RequestBody,
        gender: RequestBody,
        nik: RequestBody,
        foto_selfie: MultipartBody.Part,
        ktp: MultipartBody.Part,
        skck: MultipartBody.Part,
        certificate: MultipartBody.Part
    ) {
        ApiManager.subimitRegisMitra(token, service_id, full_name, address, phone, email, gender, nik, foto_selfie, ktp, skck, certificate)
            .doOnError{ mView?.hideProgressMessage(it) }
            .subscribe(
                Action1 { mView?.showResponseMessage(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress() }
            )
    }

    override fun subimitParticipantRegisMitra(
        token: String,
        schedule_id: RequestBody,
        full_name: RequestBody,
        address: RequestBody,
        phone: RequestBody,
        email: RequestBody,
        gender: RequestBody,
        nik: RequestBody,
        foto_selfie: MultipartBody.Part,
        ktp: MultipartBody.Part
    ) {
        ApiManager.subimitParticipantRegisMitra(token, schedule_id, full_name, address, phone, email, gender, nik, foto_selfie, ktp)
            .doOnError{ mView?.hideProgressMessage(it) }
            .subscribe(
                Action1 { mView?.showResponseMessage(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress() }
            )
    }

    override fun subimitVerif(
        token: String,
        foto_selfie: MultipartBody.Part,
        ktp: MultipartBody.Part
    ) {
        ApiManager.subimitVerif(token, foto_selfie, ktp)
            .doOnError{ mView?.hideProgressMessage(it) }
            .subscribe(
                Action1 { mView?.showResponseMessage(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress() }
            )
    }
}