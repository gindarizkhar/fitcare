package untag.f4_1461600225daskom.fitcare.ui.history

import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class HistoryPresenter: BaseMvpPresenterImpl<HistoryContract.View>(), HistoryContract.Presenter {
    override fun getAll(token: String) {
        ApiManager.getAllHistory(token)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showResponseMessage(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }
}