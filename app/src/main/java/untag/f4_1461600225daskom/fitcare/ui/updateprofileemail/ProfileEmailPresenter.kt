package untag.f4_1461600225daskom.fitcare.ui.updateprofileemail

import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class ProfileEmailPresenter: BaseMvpPresenterImpl<ProfileEmailContract.View>(), ProfileEmailContract.Presenter {

    override fun submitEmail(token: String, email: String) {
        ApiManager.editProfileEmail(token, email)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showResponse(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }

}