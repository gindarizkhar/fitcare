package untag.f4_1461600225daskom.fitcare.ui.mitra_register

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.activity_form_upload_register_mitra.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.RegisMitra
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.ui.PendaftaranMitraProgressActivity
import untag.f4_1461600225daskom.fitcare.ui.dashboard.DashboardActivity
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager
import java.io.File

class PendaftaranMitraUploadActivity : BaseMvpActivity<PendaftaranMitraUploadContract.View, PendaftaranMitraUploadContract.Presenter>(), PendaftaranMitraUploadContract.View {
    var service_id = ""
    var schedule_id = ""
    var status = ""
    var full_name =""
    var address = ""
    var phone = ""
    var email =""
    var gender = ""
    var nik = ""
    var TAG = "PENDAFTARANMITRA"
    var imageURISelfie = ""
    var imageURIKTP = ""
    var imageURISKCK = ""
    var imageURICERTIF = ""
    private lateinit var imageURISelfieFile: File
    private lateinit var imageURIKTPFile: File
    private lateinit var imageURISKCKFile: File
    private lateinit var imageURICERTIFFile: File

    val progressDialog: ProgressDialog by lazy { ProgressDialog(this) }
    override var mPresenter: PendaftaranMitraUploadContract.Presenter = PendaftaranMitraUploadPresenter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_upload_register_mitra)


        btn_kirim_form_pendaftaran.setOnClickListener{
            if (status.equals("1")) {
                val service_id_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), service_id)
                val full_name_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), full_name)
                val address_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), address)
                val phone_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), phone)
                val email_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), email)
                val gender_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), gender)
                val nik_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), nik)

                @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
                imageURISelfieFile = File(imageURISelfie)
                val fileReqBodySelfie =
                    imageURISelfieFile.asRequestBody("image/*".toMediaTypeOrNull())
                val imageSelfie = MultipartBody.Part.createFormData(
                    "foto_selfie",
                    imageURISelfieFile.name,
                    fileReqBodySelfie
                )

                imageURIKTPFile = File(imageURIKTP)
                val fileReqBodyKTP =
                    imageURIKTPFile.asRequestBody("image/*".toMediaTypeOrNull())
                val imageKTP = MultipartBody.Part.createFormData(
                    "ktp",
                    imageURIKTPFile.name,
                    fileReqBodyKTP
                )

                imageURISKCKFile = File(imageURISKCK)
                val fileReqBodySKCK =
                    imageURISKCKFile.asRequestBody("image/*".toMediaTypeOrNull())
                val imageSKCK = MultipartBody.Part.createFormData(
                    "skck",
                    imageURISKCKFile.name,
                    fileReqBodySKCK
                )

                imageURICERTIFFile = File(imageURICERTIF)
                val fileReqBodCERTIF =
                    imageURICERTIFFile.asRequestBody("image/*".toMediaTypeOrNull())
                val imageCERTIF = MultipartBody.Part.createFormData(
                    "certificate",
                    imageURICERTIFFile.name,
                    fileReqBodCERTIF
                )

                if (!imageURISelfie.equals("") && !imageURIKTP.equals("") && !imageURISKCK.equals("") && !imageURICERTIF.equals("")) {
                    PreferenceManager(this).token?.let {
                        showProgress()
                        mPresenter.subimitRegisMitra(
                            "Bearer $it",
                            service_id_part,
                            full_name_part,
                            address_part,
                            phone_part,
                            email_part,
                            gender_part,
                            nik_part,
                            imageSelfie,
                            imageKTP,
                            imageSKCK,
                            imageCERTIF
                        )
                    }
                }
            }else if (status.equals("2")){
                @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
                imageURISelfieFile = File(imageURISelfie)
                val fileReqBodySelfie =
                    imageURISelfieFile.asRequestBody("image/*".toMediaTypeOrNull())
                val imageSelfie = MultipartBody.Part.createFormData(
                    "foto_selfie",
                    imageURISelfieFile.name,
                    fileReqBodySelfie
                )

                imageURIKTPFile = File(imageURIKTP)
                val fileReqBodyKTP =
                    imageURIKTPFile.asRequestBody("image/*".toMediaTypeOrNull())
                val imageKTP = MultipartBody.Part.createFormData(
                    "ktp",
                    imageURIKTPFile.name,
                    fileReqBodyKTP
                )



                if (!imageURISelfie.equals("") && !imageURIKTP.equals("") ) {
                    PreferenceManager(this).token?.let {
                        showProgress()
                        mPresenter.subimitVerif(
                            "Bearer $it",
                            imageSelfie,
                            imageKTP
                        )
                    }
                }
            }else{
                val schedule_id_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), schedule_id)
                val full_name_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), full_name)
                val address_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), address)
                val phone_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), phone)
                val email_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), email)
                val gender_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), gender)
                val nik_part = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), nik)

                @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
                imageURISelfieFile = File(imageURISelfie)
                val fileReqBodySelfie =
                    imageURISelfieFile.asRequestBody("image/*".toMediaTypeOrNull())
                val imageSelfie = MultipartBody.Part.createFormData(
                    "foto_selfie",
                    imageURISelfieFile.name,
                    fileReqBodySelfie
                )

                imageURIKTPFile = File(imageURIKTP)
                val fileReqBodyKTP =
                    imageURIKTPFile.asRequestBody("image/*".toMediaTypeOrNull())
                val imageKTP = MultipartBody.Part.createFormData(
                    "ktp",
                    imageURIKTPFile.name,
                    fileReqBodyKTP
                )

                if (!imageURISelfie.equals("") && !imageURIKTP.equals("")) {
                    PreferenceManager(this).token?.let {
                        showProgress()
                        mPresenter.subimitParticipantRegisMitra(
                            "Bearer $it",
                            schedule_id_part,
                            full_name_part,
                            address_part,
                            phone_part,
                            email_part,
                            gender_part,
                            nik_part,
                            imageSelfie,
                            imageKTP
                        )
                    }
                }
            }





        }

        setToolbar()
        init()

        iv_fotoselfi.setOnClickListener{
            ImagePicker.with(this)
                .compress(1024)
                .maxResultSize(720, 720)
                .saveDir(
                    File(
                        Environment.getExternalStorageDirectory(),
                        "Fit Care"
                    )
                )
                .start { resultCode, data ->
                    if (resultCode == Activity.RESULT_OK) {
                        val fileUri = data?.data
                        iv_fotoselfi.setImageURI(fileUri)
                        //You can also get File Path from intent
                        imageURISelfie = ImagePicker.getFilePath(data).toString()
                        Log.d(TAG, imageURISelfie)
                    }
                }
        }

        iv_ktp.setOnClickListener{
            ImagePicker.with(this)
                .compress(1024)
                .maxResultSize(720, 720)
                .saveDir(
                    File(
                        Environment.getExternalStorageDirectory(),
                        "Fit Care"
                    )
                )
                .start { resultCode, data ->
                    if (resultCode == Activity.RESULT_OK) {
                        val fileUri = data?.data
                        iv_ktp.setImageURI(fileUri)
                        //You can also get File Path from intent
                        imageURIKTP = ImagePicker.getFilePath(data).toString()
                        Log.d(TAG, imageURIKTP)
                    }
                }
        }

        if (status.equals("1")) {
            iv_skck.setOnClickListener {
                ImagePicker.with(this)
                    .compress(1024)
                    .maxResultSize(720, 720)
                    .saveDir(
                        File(
                            Environment.getExternalStorageDirectory(),
                            "Fit Care"
                        )
                    )
                    .start { resultCode, data ->
                        if (resultCode == Activity.RESULT_OK) {
                            val fileUri = data?.data
                            iv_skck.setImageURI(fileUri)
                            //You can also get File Path from intent
                            imageURISKCK = ImagePicker.getFilePath(data).toString()
                            Log.d(TAG, imageURISKCK)
                        }
                    }
            }

            iv_sertif.setOnClickListener {
                ImagePicker.with(this)
                    .compress(1024)
                    .maxResultSize(720, 720)
                    .saveDir(
                        File(
                            Environment.getExternalStorageDirectory(),
                            "Fit Care"
                        )
                    )
                    .start { resultCode, data ->
                        if (resultCode == Activity.RESULT_OK) {
                            val fileUri = data?.data
                            iv_sertif.setImageURI(fileUri)
                            //You can also get File Path from intent
                            imageURICERTIF = ImagePicker.getFilePath(data).toString()
                            Log.d(TAG, imageURICERTIF)
                        }
                    }
            }
        }
    }

    private fun init(){
        status =  getIntent().getStringExtra("status")
        if (status.equals("1")) {
            service_id = getIntent().getStringExtra("service_id")
        }else if (status.equals("2")) {
            cv_sertif.visibility = View.GONE
            cv_skck.visibility = View.GONE
            toolbar.title = "Verifikasi Identitas"
        }else{
            schedule_id = getIntent().getStringExtra("schedule_id")
            cv_sertif.visibility = View.GONE
            cv_skck.visibility = View.GONE
        }

        if (!status.equals("2")) {
            full_name = getIntent().getStringExtra("full_name")
            address = getIntent().getStringExtra("address")
            phone = getIntent().getStringExtra("phone")
            email = getIntent().getStringExtra("email")
            gender = getIntent().getStringExtra("gender")
            nik = getIntent().getStringExtra("nik")
        }
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
    }

    private fun showProgress() {
        progressDialog.setMessage("Uploading...")
        progressDialog.show()
    }

    override fun showResponseMessage(response: RegisMitra.RegisMitraResponse) {
        progressDialog.dismiss()
        if (response.meta.code == 200){
            finishAffinity()
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
            val btn_kirim = Intent(
                this,
                PendaftaranMitraProgressActivity::class.java
            )
            startActivity(btn_kirim)
        }else if(response.meta.code == 400){
            Toast.makeText(this,response.meta.message.toString(),Toast.LENGTH_LONG).show()
        }else {
            Toast.makeText(this,"Gagal upload data",Toast.LENGTH_LONG).show()
        }
    }

    override fun hideProgress() {
        progressDialog.dismiss()
    }

    override fun hideProgressMessage(it: Throwable?) {
        if (status.equals("2")){
            Toast.makeText(
                this,
                "Your verification will be done, please wait",
                Toast.LENGTH_LONG
            ).show()
            finish()
        }else if (status.equals("1")){
            Toast.makeText(
                this,
                "No hp/ email sudah digunakan atau periksa kembali data anda",
                Toast.LENGTH_LONG
            ).show()
        }else{
            Toast.makeText(
                this,
                "Periksa saldo anda",
                Toast.LENGTH_LONG
            ).show()
        }
    }

}
