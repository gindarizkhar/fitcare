package untag.f4_1461600225daskom.fitcare.ui.login

import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class LoginPresenter: BaseMvpPresenterImpl<LoginContract.View>(),LoginContract.Presenter {
    override fun submitPhoneNumber(phone: String) {
        ApiManager.login(phone)
            .doOnError{mView?.showMessageErrorLogin()}
            .subscribe(
                Action1 { mView?.showResponseMessage(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }
}