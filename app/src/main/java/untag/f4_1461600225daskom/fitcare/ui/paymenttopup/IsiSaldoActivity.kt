package untag.f4_1461600225daskom.fitcare.ui.paymenttopup

import android.os.Bundle
import android.widget.Toast
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback
import com.midtrans.sdk.corekit.core.*
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme
import com.midtrans.sdk.corekit.models.*
import com.midtrans.sdk.corekit.models.snap.CreditCard
import com.midtrans.sdk.corekit.models.snap.TransactionResult
import com.midtrans.sdk.uikit.SdkUIFlowBuilder
import kotlinx.android.synthetic.main.activity_isi_saldo.*
import kotlinx.android.synthetic.main.activity_term_jadi_mitra.toolbar
import untag.f4_1461600225daskom.fitcare.BuildConfig.BASE_URL_MIDTRANS
import untag.f4_1461600225daskom.fitcare.BuildConfig.CLIENT_KEY
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.Profile
import untag.f4_1461600225daskom.fitcare.model.TopUp
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager
import java.text.SimpleDateFormat
import java.util.*

class IsiSaldoActivity : BaseMvpActivity<IsiSaldoContract.View, IsiSaldoContract.Presenter>(), IsiSaldoContract.View,
    TransactionFinishedCallback {
    private val TAG = "IsiSaldoActivity"
    private var full_name = ""
    private var email = ""
    private var phone = ""
    private var order_id = ""
    override var mPresenter: IsiSaldoContract.Presenter = IsiSaldoPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_isi_saldo)

        setToolbar()

        makePayment()


        lanjut.setOnClickListener {
//            val lanjut = Intent(this,PaymentTopupActivity::class.java)
//            lanjut.putExtra("nominal",nominal_saldo.text.toString())
//            startActivity(lanjut)
            if (nominal_saldo.text.isNotEmpty()){
                if (nominal_saldo.text.toString().toInt() < 100000){
                    Toast.makeText(this, "Mohon maaf, minimal topup saldo adalah Rp. 100000", Toast.LENGTH_LONG).show()
                }else {
                    PreferenceManager(this).token?.let {
                        mPresenter.insertMidtrans("Bearer $it", nominal_saldo.text.toString(), "BNI")
                    }
                }
            }else{
                Toast.makeText(this, "Jangan lupa isi nominal saldonya", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
    }

    private fun clickPay() {

        UserDetailInit()
        MidtransSDK.getInstance().transactionRequest = null
        MidtransSDK.getInstance().transactionRequest = transactionRequest()
        MidtransSDK.getInstance()
            .startPaymentUiFlow(this, PaymentMethod.BANK_TRANSFER_OTHER)
    }

    private fun makePayment() {
        SdkUIFlowBuilder.init()
            .setContext(this)
            .setMerchantBaseUrl(BASE_URL_MIDTRANS)
            .setClientKey(CLIENT_KEY)
            .setTransactionFinishedCallback(this)
            .enableLog(true)
            .setColorTheme(CustomColorTheme("#777777", "#f77474", "#3f0d0d"))
            .buildSDK()
    }

    fun customerDetails(): CustomerDetails? {
        val cd =
            CustomerDetails()
        cd.firstName =
            full_name
        cd.email = email
        cd.phone = phone
        return cd
    }

    fun expiri(): ExpiryModel? {
        val expiryModel = ExpiryModel()
        expiryModel.unit = ExpiryModel.UNIT_HOUR
        expiryModel.duration = 5
        return expiryModel
    }

    fun UserDetailInit() {
        PreferenceManager(this).token?.let {
            mPresenter.getProfile("Bearer $it")
        }

    }

    fun transactionRequest(): TransactionRequest? {
        val total = 4000.0 + nominal_saldo.text.toString().toDouble()

        val request = TransactionRequest(order_id, total
        )
        request.setCustomerDetails(customerDetails()!!)
        val itemDetails =
            ArrayList<ItemDetails>()
        val nominal =
            ItemDetails(
                "nominal_topup",
                nominal_saldo.text.toString().toDouble(),
                1,
                "Nominal Topup"
            )
        itemDetails.add(nominal)
        val details =
            ItemDetails(
                "biaya_admin",
                4000.0,
                1,
                "Biaya Admin"
            )
        itemDetails.add(details)
        request.itemDetails = itemDetails
        request.expiry = expiri()
        val creditCard = CreditCard()
        creditCard.isSaveCard = false
        creditCard.authentication = CreditCard.AUTHENTICATION_TYPE_RBA
        request.creditCard = creditCard
        return request
    }

    override fun showResponse(response: Profile.ProfileResponse) {
        full_name = response.data.user.user_data.full_name
        phone = response.data.user.phone
        email =  response.data.user.email

        if (response.data.user.user_data.address.isNullOrEmpty() ) {
            val userDetail = UserDetail()

            userDetail.userFullName = response.data.user.user_data.full_name

            userDetail.email =
                response.data.user.email
            userDetail.phoneNumber =
                response.data.user.phone
            // set user ID as identifier of saved card (can be anything as long as unique),
            // randomly generated by SDK if not supplied
//            userDetail.setUserId("budi-6789");
            val userAddresses = ArrayList<UserAddress>()
            val userAddress = UserAddress()
            userAddress.address =
                "Indonesia"

            userAddress.addressType = Constants.ADDRESS_TYPE_BOTH
            userAddress.country = "IDN"
            userAddresses.add(userAddress)
            userDetail.userAddresses = userAddresses
            LocalDataHandler.saveObject("user_details", userDetail)
        }else{
            val userDetail = UserDetail()

            userDetail.userFullName = response.data.user.user_data.full_name

            userDetail.email =
                response.data.user.email
            userDetail.phoneNumber =
                response.data.user.phone
            // set user ID as identifier of saved card (can be anything as long as unique),
            // randomly generated by SDK if not supplied
//            userDetail.setUserId("budi-6789");
            val userAddresses = ArrayList<UserAddress>()
            val userAddress = UserAddress()
            userAddress.address =
                response.data.user.user_data.address

            userAddress.addressType = Constants.ADDRESS_TYPE_BOTH
            userAddress.country = "IDN"
            userAddresses.add(userAddress)
            userDetail.userAddresses = userAddresses
            LocalDataHandler.saveObject("user_details", userDetail)
        }
    }

    override fun showResponseMidtrans(response: TopUp.TopUpResponse) {
        order_id = response.data.wallet_charge.transaction_code
        clickPay()
    }


    override fun hideProgress() {
    }

    override fun hideProgressUpload() {
    }

    override fun showMessageErrorLogin() {
    }

    override fun showMessageOTP() {
    }

    override fun onTransactionFinished(result: TransactionResult?) {
        if (result != null) {
            if (result.getResponse() != null) {

                result.getResponse().getValidationMessages()
                finish()
            } else if (result.isTransactionCanceled()) {
                Toast.makeText(this, "Transaction Failed", Toast.LENGTH_LONG).show()
            } else {
                if (result.getStatus().equals(TransactionResult.STATUS_INVALID, ignoreCase = true)) {
                    Toast.makeText(
                        this,
                        "Transaction Invalid" + result.getResponse().getTransactionId(),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    Toast.makeText(this, "Something Wrong", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}