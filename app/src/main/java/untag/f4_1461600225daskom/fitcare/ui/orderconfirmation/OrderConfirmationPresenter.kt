package untag.f4_1461600225daskom.fitcare.ui.orderconfirmation

import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class OrderConfirmationPresenter: BaseMvpPresenterImpl<OrderConfirmationContract.View>(), OrderConfirmationContract.Presenter {
    override fun submitOrder(
        token: String,
        serviceId: String,
        longitude: String,
        latitude: String,
        time: String,
        note: String,
        duration: String,
        payment: String
    ) {
        ApiManager.submitOrder(token, serviceId, longitude, latitude, time, note, duration, payment)
            .doOnError{mView?.showResponseErrorMessage(it)}
            .subscribe(
                Action1 { mView?.showResponseMessage(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }
}