package untag.f4_1461600225daskom.fitcare.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.login_activity.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.Login
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.ui.otp.OtpActivity
import untag.f4_1461600225daskom.fitcare.ui.register.RegisterActivity
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager

class LoginActivity : BaseMvpActivity<LoginContract.View,LoginContract.Presenter>(),LoginContract.View {
    override var mPresenter: LoginContract.Presenter=LoginPresenter()

    private var phonenumberlogin=""
    override fun showResponseMessage(response: Login.LoginResponse) {
        pg_login.visibility = View.INVISIBLE
        btnlogin.visibility = View.VISIBLE
        if(response.meta.code==200) {
            val intent = Intent(this, OtpActivity::class.java)
            intent.putExtra("from", "login")
            intent.putExtra("phone", nohplogin.text.toString())
            startActivity(intent)
            Toast.makeText(this,"Login Berhasil",Toast.LENGTH_SHORT).show()
            finish()
        }else{
            Toast.makeText(this,"No Telepon tidak ditemukan",Toast.LENGTH_SHORT).show()
        }
    }

    override fun hideProgress() {
        pg_login.visibility = View.INVISIBLE
        btnlogin.visibility = View.VISIBLE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        Glide.with(this).load(R.drawable.logo).into(imageviewlogin)
        phonenumberlogin=nohplogin.text.toString()

        btnlogin.setOnClickListener {
            if (nohplogin.text.isNullOrBlank()) {
                Toast.makeText(this, "Isikan Nomer HP anda ", Toast.LENGTH_SHORT).show()
            } else{
                pg_login.visibility = View.VISIBLE
                btnlogin.visibility = View.INVISIBLE
                mPresenter.submitPhoneNumber(nohplogin.text.toString())
            }
        }

        tv_daftar.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        })
    }

}
