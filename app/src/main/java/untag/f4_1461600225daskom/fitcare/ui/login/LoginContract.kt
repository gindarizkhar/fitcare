package untag.f4_1461600225daskom.fitcare.ui.login

import untag.f4_1461600225daskom.fitcare.model.Login
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface LoginContract{
    interface View: BaseMvpView{
        fun showResponseMessage(response: Login.LoginResponse)

        fun hideProgress()
    }
    interface Presenter: BaseMvpPresenter<View>{
        fun submitPhoneNumber(phone: String)
    }
}