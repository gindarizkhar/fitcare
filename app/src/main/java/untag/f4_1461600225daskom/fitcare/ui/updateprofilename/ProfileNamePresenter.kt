package untag.f4_1461600225daskom.fitcare.ui.updateprofilename

import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class ProfileNamePresenter: BaseMvpPresenterImpl<ProfileNameContract.View>(), ProfileNameContract.Presenter {
    override fun submitName(token: String, fullName: String) {
        ApiManager.updateProfileName(token, fullName)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showResponse(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }
}