package untag.f4_1461600225daskom.fitcare.ui.history

import untag.f4_1461600225daskom.fitcare.model.History
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface HistoryContract {
    interface View: BaseMvpView {
        fun showResponseMessage(response: History.HistoryResponse)

        fun hideProgress()
    }

    interface Presenter: BaseMvpPresenter<View> {
        fun getAll(token: String)
    }
}