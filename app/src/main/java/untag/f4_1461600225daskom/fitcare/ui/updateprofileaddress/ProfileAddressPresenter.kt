package untag.f4_1461600225daskom.fitcare.ui.updateprofileaddress

import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class ProfileAddressPresenter: BaseMvpPresenterImpl<ProfileAddressContract.View>(), ProfileAddressContract.Presenter {

    override fun submitAddress(token: String, address: String) {
        ApiManager.updateProfileAddress(token, address)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showResponse(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }

}