package untag.f4_1461600225daskom.fitcare.ui.historydetail

import okhttp3.MultipartBody
import okhttp3.RequestBody
import untag.f4_1461600225daskom.fitcare.model.Dashboard
import untag.f4_1461600225daskom.fitcare.model.History
import untag.f4_1461600225daskom.fitcare.model.HistoryDetail
import untag.f4_1461600225daskom.fitcare.model.RegisMitra
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface HistoryDetailContract {
    interface View: BaseMvpView {
        fun showResponseMessage(response: HistoryDetail.HistoryResponse)

        fun showResponseMessageDetail(response: HistoryDetail.HistoryResponse)


        fun hideProgress()
        fun hideProgressMessage(it: Throwable?)
    }
    interface Presenter: BaseMvpPresenter<View> {
        fun submitSelesai(authorization: String,
                              id: String

        )

        fun getDetail(authorization: String,
                          id: String

        )
    }
}