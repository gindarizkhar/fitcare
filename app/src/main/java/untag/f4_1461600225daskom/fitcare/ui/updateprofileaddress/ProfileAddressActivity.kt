package untag.f4_1461600225daskom.fitcare.ui.updateprofileaddress

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit_profile.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.Profile
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager

class ProfileAddressActivity : BaseMvpActivity<ProfileAddressContract.View, ProfileAddressContract.Presenter>(), ProfileAddressContract.View {

    override var mPresenter: ProfileAddressContract.Presenter = ProfileAddressPresenter()

    override fun showResponse(response: Profile.UpdateAddressResponse) {
        if (response.meta.code == 200) finish()
    }

    override fun hideProgress() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        initView()

        button_save.setOnClickListener {
            PreferenceManager(this).token?.let {
                mPresenter.submitAddress("Bearer $it", et_input.text.toString())
            }
        }
    }

    private fun initView() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
        toolbar_title.text = "Edit Address"
        ti_input.hint = "Input Alamat"
        et_input.setText(intent.getStringExtra("address"))
    }
}
