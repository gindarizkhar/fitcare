package untag.f4_1461600225daskom.fitcare.ui.updateprofileemail

import untag.f4_1461600225daskom.fitcare.model.Profile
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface ProfileEmailContract {
    interface View: BaseMvpView {
        fun showResponse(response: Profile.UpdateEmailResponse)

        fun hideProgress()
    }

    interface Presenter: BaseMvpPresenter<View> {
        fun submitEmail(token: String, email: String)
    }
}