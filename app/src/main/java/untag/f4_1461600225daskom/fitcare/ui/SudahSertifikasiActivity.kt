package untag.f4_1461600225daskom.fitcare.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_sudah_sertifikasi.*
import kotlinx.android.synthetic.main.activity_term_jadi_mitra.toolbar
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.BelumSertifikasi
import untag.f4_1461600225daskom.fitcare.ui.belumsertifikasi.belum_sertifikasi
import untag.f4_1461600225daskom.fitcare.ui.mitra_register.PendaftaranMitraActivity


class SudahSertifikasiActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sudah_sertifikasi)

        setToolbar()

        cv_sudah_sertif.setOnClickListener {
            val sudah = Intent(this,
                PendaftaranMitraActivity::class.java)
            sudah.putExtra("service_id",getIntent().getStringExtra("service_id"))
            sudah.putExtra("status","1")
            startActivity(sudah)
        }

        cv_belum_sertif.setOnClickListener {
            val belum = Intent(this,
                belum_sertifikasi::class.java)
            belum.putExtra("service_id",getIntent().getStringExtra("service_id"))
            startActivity(belum)
        }
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
    }
}