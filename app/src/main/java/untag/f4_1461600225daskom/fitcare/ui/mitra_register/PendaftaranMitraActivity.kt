package untag.f4_1461600225daskom.fitcare.ui.mitra_register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.RadioButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_register_massage.*
import kotlinx.android.synthetic.main.activity_term_jadi_mitra.toolbar
import untag.f4_1461600225daskom.fitcare.R

class PendaftaranMitraActivity : AppCompatActivity() {
    var gender = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_massage)

        setToolbar()

        btnregister.setOnClickListener {
            var id: Int = RGgender.checkedRadioButtonId
            if (id!=-1){ // If any radio button checked from radio group
                // Get the instance of radio button using id
                val radio: RadioButton = findViewById(id)
//                Toast.makeText(applicationContext,"On button click : ${radio.text}",
//                    Toast.LENGTH_SHORT).show()
                gender = radio.text.toString()
            }else{
                // If no radio button checked in this radio group
                Toast.makeText(applicationContext,"Pilih jenis kelamin Anda",
                    Toast.LENGTH_SHORT).show()
            }

            if (namalengkap.text.toString().isNullOrEmpty()) {
                namalengkap.setError("Wajib diisi")
            } else if (alamat.text.toString().isNullOrEmpty()) {
                alamat.setError("Wajib diisi")
            } else if (nohp.text.toString().isNullOrEmpty()) {
                nohp.setError("Wajib diisi")
            } else if (email.text.toString().isNullOrEmpty()) {
                email.setError("Wajib diisi")
            } else if (nik.text.toString().isNullOrEmpty()) {
                nik.setError("Wajib diisi")
            } else{
                val lanjut = Intent(this, PendaftaranMitraUploadActivity::class.java)
                lanjut.putExtra("service_id", getIntent().getStringExtra("service_id"))
                lanjut.putExtra("schedule_id", getIntent().getStringExtra("schedule_id"))
                lanjut.putExtra("status", getIntent().getStringExtra("status"))
                lanjut.putExtra("full_name", namalengkap.text.toString())
                lanjut.putExtra("address", alamat.text.toString())
                lanjut.putExtra("phone", nohp.text.toString())
                lanjut.putExtra("email", email.text.toString())
                lanjut.putExtra("gender", gender)
                lanjut.putExtra("nik",  nik.text.toString())
                startActivity(lanjut)
            }
        }
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
    }
}
