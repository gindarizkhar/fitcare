package untag.f4_1461600225daskom.fitcare.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_jadi_mitra.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.fragment_home.view.isi_saldo
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.Home
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpFragment
import untag.f4_1461600225daskom.fitcare.ui.DepositActivity
import untag.f4_1461600225daskom.fitcare.ui.JadiMitraActivity
import untag.f4_1461600225daskom.fitcare.ui.TermJadiMitraActivity
import untag.f4_1461600225daskom.fitcare.ui.TermsConditionActivity
import untag.f4_1461600225daskom.fitcare.ui.login.LoginActivity
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager

class HomeFragment : BaseMvpFragment<HomeContract.View, HomeContract.Presenter>(), HomeContract.View {

    private lateinit var root: View
    private val adapter = HomeAdapter()

    override var mPresenter: HomeContract.Presenter = HomePresenter()

    override fun showResponseMessage(response: Home.HomeResponse) {
        if (response.meta.code == 200) {
            root.nama_customer.text = "Hai, ${response.data.user.user_data.full_name}"
            adapter.addAll(response.data.news)

            isi_saldo.text= "Rp${response.data.wallet.amount}"

            root.topup_saldo.setOnClickListener {
                if (response.data.wallet.status.equals("1")) {
                    val deposit = Intent(
                        context,
                        DepositActivity::class.java
                    )
                    deposit.putExtra("amount",response.data.wallet.amount)
                    startActivity(deposit)
                }else{
                    Toast.makeText(context,"Silahkan verifikasi identitas Anda di menu profil", Toast.LENGTH_LONG).show()
                }
            }
        }else if (response.meta.code == 401){
            PreferenceManager(context).clearData()
            val logout = Intent(context, LoginActivity::class.java)
            startActivity(logout)
        }
    }

    override fun hideProgress() {
    }

    override fun showMessageErrorLogin() {
    }

    override fun showMessageOTP() {
        TODO("Not yet implemented")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        root = inflater.inflate(R.layout.fragment_home, container, false)

        initRecyler()

        val token = PreferenceManager(context).token
        token?.let {
            mPresenter.getAll("Bearer $it")
        }

//        val service:MutableList<Home.Service> = mutableListOf()
//        service.add(Home.Service(1, "Spa++", "Spa murah meriah", "................"))
//        adapter.addAllFitur(service)

        root.baby_spa.setOnClickListener {
            val babyspa = Intent(context, TermsConditionActivity::class.java)
            babyspa.putExtra("service_id", 1)
            startActivity(babyspa)
        }

        root.mom_spa.setOnClickListener {
            val momspa = Intent(context, TermsConditionActivity::class.java)
            momspa.putExtra("service_id", 2)
            startActivity(momspa)
        }

        root.kids_spa.setOnClickListener {
            val kids_spa = Intent(context, TermsConditionActivity::class.java)
            kids_spa.putExtra("service_id", 3)
            startActivity(kids_spa)
        }

        root.dad_spa.setOnClickListener {
            val dad_spa = Intent(context, TermsConditionActivity::class.java)
            dad_spa.putExtra("service_id", 4)
            startActivity(dad_spa)
        }



        root.jadimitra.setOnClickListener {
            val jadimitra = Intent(context, JadiMitraActivity::class.java)
            startActivity(jadimitra)
        }
        return root
    }

    private fun initRecyler(){
        root.rv_main_home.adapter = adapter
        root.rv_main_home.layoutManager = LinearLayoutManager(context)
    }
}