package untag.f4_1461600225daskom.fitcare.ui.updateprofileemail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit_profile.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.Profile
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager

class ProfileEmailActivity : BaseMvpActivity<ProfileEmailContract.View, ProfileEmailContract.Presenter>(), ProfileEmailContract.View {

    override var mPresenter: ProfileEmailContract.Presenter = ProfileEmailPresenter()

    override fun showResponse(response: Profile.UpdateEmailResponse) {
        if (response.meta.code == 200) finish()
    }

    override fun hideProgress() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        initView()

        button_save.setOnClickListener {
            PreferenceManager(this).token?.let {
                mPresenter.submitEmail("Bearer $it", et_input.text.toString())
            }
        }
    }

    private fun initView() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
        toolbar_title.text = "Edit Email"
        ti_input.hint = "Input Email"
        et_input.setText(intent.getStringExtra("email"))
    }
}
