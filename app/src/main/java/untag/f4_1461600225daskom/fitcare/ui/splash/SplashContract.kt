package untag.f4_1461600225daskom.fitcare.ui.splash

import untag.f4_1461600225daskom.fitcare.model.OtpResponse
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface SplashContract {
    interface View: BaseMvpView {
        fun showResponse(response: OtpResponse)

        fun closeApp()
    }
    interface Presenter: BaseMvpPresenter<View> {
        fun otpRegister(phone:String, otp:Int)
    }
}