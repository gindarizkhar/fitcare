package untag.f4_1461600225daskom.fitcare.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_term_jadi_mitra.*
import untag.f4_1461600225daskom.fitcare.R

class TermJadiMitraActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_term_jadi_mitra)

        setToolbar()


        tv_judul.text = "Lorem Ipsum"
        tv_keterangan.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis euismod mauris. Vestibulum facilisis, enim eu pharetra dictum, urna elit accumsan lorem, sed hendrerit mauris diam et ligula. Sed suscipit risus quis leo porta fermentum. Phasellus ornare ex arcu, non bibendum neque vehicula in. Aenean at fermentum leo. Sed ac quam sed libero lobortis laoreet. Sed vitae tellus vulputate, mattis tortor id, finibus massa. Duis faucibus iaculis libero, sed mollis diam dignissim sed. Nulla fringilla feugiat nisi sed hendrerit. Integer viverra pulvinar neque vitae sodales. Pellentesque id ultricies sapien. Nam et consequat eros, quis gravida libero. Aliquam eu accumsan ante. Aenean gravida orci non nisi vulputate imperdiet. Aenean dapibus elit non tellus mattis, non porta mauris aliquet. Nulla laoreet gravida magna, vestibulum tincidunt ante ultricies non.\n" +
                "\n" +
                "Pellentesque id velit suscipit, feugiat lorem id, cursus ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent malesuada nulla posuere mauris dignissim, eu semper nunc efficitur. Curabitur interdum sit amet metus vel facilisis. Donec vel pulvinar arcu. Sed eleifend pharetra luctus. Etiam nec vestibulum felis. Vestibulum id velit ac quam mattis semper. Maecenas vel metus in diam molestie vehicula nec a turpis. Duis vel nibh in nisi ornare varius. Donec vitae facilisis ipsum, id elementum purus. Duis iaculis eu erat in euismod."

        cb_agree.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                cv_lanjut.setOnClickListener {
                    val sertif = Intent(this,SudahSertifikasiActivity::class.java)
                    sertif.putExtra("service_id",getIntent().getStringExtra("service_id"))
                    startActivity(sertif)
                }
            }else{
                cv_lanjut.setOnClickListener(null)
            }
        }
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
    }
}