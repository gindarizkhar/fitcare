package untag.f4_1461600225daskom.fitcare.ui.historydetail

import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class HistoryDetailPresenter: BaseMvpPresenterImpl<HistoryDetailContract.View>(), HistoryDetailContract.Presenter {

    override fun submitSelesai(
        authorization: String,
        id: String
    ) {
        ApiManager.submitSelesai(authorization,id)
            .doOnError{ mView?.hideProgressMessage(it) }
            .subscribe(
                Action1 { mView?.showResponseMessage(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress() }
            )
    }

    override fun getDetail(token: String, id: String) {
        ApiManager.getDetailHistory(token,id)
            .doOnError{mView?.hideProgress()}
            .subscribe(
                Action1 { mView?.showResponseMessageDetail(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }
}