package untag.f4_1461600225daskom.fitcare.ui.updateprofilename

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit_profile.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager
import untag.f4_1461600225daskom.fitcare.model.Profile
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity

class ProfileNameActivity : BaseMvpActivity<ProfileNameContract.View, ProfileNameContract.Presenter>(), ProfileNameContract.View {

    override var mPresenter: ProfileNameContract.Presenter = ProfileNamePresenter()

    override fun showResponse(response: Profile.UpdateNameResponse) {
        if (response.meta.code == 200) finish()
    }

    override fun hideProgress() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        initView()

        button_save.setOnClickListener {
            PreferenceManager(this).token?.let {
                mPresenter.submitName("Bearer $it", et_input.text.toString())
            }
        }
    }

    private fun initView() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
        toolbar_title.text = "Edit Name"
        ti_input.hint = "Input Nama"
        et_input.setText(intent.getStringExtra("full_name"))
    }
}
