package untag.f4_1461600225daskom.fitcare.ui.paymenttopup_history


import okhttp3.MultipartBody
import untag.f4_1461600225daskom.fitcare.model.Profile
import untag.f4_1461600225daskom.fitcare.model.TopUp
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface TopupHistoryContract {
    interface View: BaseMvpView {
        fun showResponse(response: TopUp.TopUpHistoryResponse)


        fun hideProgress()

    }
    interface Presenter: BaseMvpPresenter<View>{
        fun getTopupHistory(token: String)
    }
}