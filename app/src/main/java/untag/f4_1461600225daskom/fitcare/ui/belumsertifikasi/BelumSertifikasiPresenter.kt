package untag.f4_1461600225daskom.fitcare.ui.belumsertifikasi

import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class BelumSertifikasiPresenter: BaseMvpPresenterImpl<BelumSertifikasiContract.View>(), BelumSertifikasiContract.Presenter {

    override fun getSertifikasi(authorization: String) {
        ApiManager.getSertifikasi(authorization)
            .doOnError{ mView?.hideProgressMessage(it) }
            .subscribe(
                Action1 { mView?.showResponseMessage(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress() }
            )
    }

    override fun getSertifikasiDetail(authorization: String, id: String) {
        ApiManager.getSertifikasiDetail(authorization,id)
            .doOnError{ mView?.hideProgressMessage(it) }
            .subscribe(
                Action1 { mView?.showResponseMessageDetail(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress() }
            )
    }
}