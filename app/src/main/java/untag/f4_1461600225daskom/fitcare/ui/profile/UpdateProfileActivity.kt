package untag.f4_1461600225daskom.fitcare.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_update_profile.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager
import untag.f4_1461600225daskom.fitcare.model.Profile
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.ui.updateprofileaddress.ProfileAddressActivity
import untag.f4_1461600225daskom.fitcare.ui.updateprofileemail.ProfileEmailActivity
import untag.f4_1461600225daskom.fitcare.ui.updateprofilephone.ProfilePhoneActivity
import untag.f4_1461600225daskom.fitcare.ui.updateprofilename.ProfileNameActivity

class UpdateProfileActivity : BaseMvpActivity<ProfileContract.View, ProfileContract.Presenter>(), ProfileContract.View {

    override var mPresenter: ProfileContract.Presenter = ProfilePresenter()

    override fun showResponse(response: Profile.ProfileResponse) {
        nama_update_bio.text = response.data.user.user_data.full_name
        alamat_bio.text = response.data.user.user_data.address
        nomor_hp_bio.text = response.data.user.phone
        alamat_email_bio.text = response.data.user.email

        val verifStatus = response.data.user.user_verification_status.status
        if (verifStatus == "1") {
            tv_send_verification.visibility = View.VISIBLE
            tv_send_verification.setOnClickListener {
                PreferenceManager(this).token?.let {
                    mPresenter.verifyEmail("Bearer $it")
                }
            }

            tv_verification_status.setTextColor(ContextCompat.getColor(this, R.color.unverified))
            tv_verification_status.text = "Belum terverifikasi"
        } else if (verifStatus == "3") {
            tv_send_verification.visibility = View.GONE
            tv_verification_status.setTextColor(ContextCompat.getColor(this, R.color.send_verification))
            tv_verification_status.text = "Telah terverifikasi"
        }
    }

    override fun showUploadResponse(response: Profile.UploadProfileImageResponse) {
    }

    override fun showVerifyResponse(response: Profile.VerifyEmailResponse) {
        if (response.meta.code == 200)
            Toast.makeText(this, "Email verifikasi telah dikirim", Toast.LENGTH_SHORT).show()
    }

    override fun hideProgress() {
    }

    override fun hideProgressUpload() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_profile)

        setToolbar()

        nama_update_bio.text = intent.getStringExtra("full_name")
        alamat_bio.text = intent.getStringExtra("address")
        nomor_hp_bio.text = intent.getStringExtra("phone")
        alamat_email_bio.text = intent.getStringExtra("email")

        layout_1.setOnClickListener {
            startActivity(
                Intent(this, ProfileNameActivity::class.java).apply {
                    putExtra("full_name", nama_update_bio.text.toString())
                }
            )
        }

        layout_2.setOnClickListener {
            startActivity(
                Intent(this, ProfileAddressActivity::class.java).apply {
                    putExtra("address", alamat_bio.text.toString())
                }
            )
        }

        layout_3.setOnClickListener {
            startActivity(
                Intent(this, ProfilePhoneActivity::class.java).apply {
                    putExtra("phone", nomor_hp_bio.text.toString())
                }
            )
        }

        layout_4.setOnClickListener {
            startActivity(
                Intent(this, ProfileEmailActivity::class.java).apply {
                    putExtra("email", alamat_email_bio.text.toString())
                }
            )
        }
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun onResume() {
        super.onResume()
        PreferenceManager(this).token?.let {
            mPresenter.getProfile("Bearer "+it)
        }
    }
}