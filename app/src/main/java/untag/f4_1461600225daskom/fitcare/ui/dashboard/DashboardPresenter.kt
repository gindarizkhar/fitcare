package untag.f4_1461600225daskom.fitcare.ui.dashboard

import okhttp3.MultipartBody
import okhttp3.RequestBody
import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class DashboardPresenter: BaseMvpPresenterImpl<DashboardContract.View>(), DashboardContract.Presenter {

    override fun submitFCM(
        authorization: String,
        token: String
    ) {
        ApiManager.submitFCM(authorization,token)
            .doOnError{ mView?.hideProgressMessage(it) }
            .subscribe(
                Action1 { mView?.showResponseMessage(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress() }
            )
    }
}