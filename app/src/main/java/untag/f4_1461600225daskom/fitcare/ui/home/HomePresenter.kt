package untag.f4_1461600225daskom.fitcare.ui.home

import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class HomePresenter : BaseMvpPresenterImpl<HomeContract.View>(),HomeContract.Presenter{
    override fun getAll(token: String) {
        ApiManager.home(token)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showResponseMessage(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }
}