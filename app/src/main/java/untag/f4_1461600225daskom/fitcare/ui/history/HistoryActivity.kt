package untag.f4_1461600225daskom.fitcare.ui.history

import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_history.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.History
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager

class HistoryActivity : BaseMvpActivity<HistoryContract.View, HistoryContract.Presenter>(), HistoryContract.View {

    private val adapter = HistoryAdapter()
    override var mPresenter: HistoryContract.Presenter = HistoryPresenter()

    override fun showResponseMessage(response: History.HistoryResponse) {
        adapter.addAllHistory(response.data.order)
    }

    override fun hideProgress() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        setToolbar()
        setRecyclerView()

        PreferenceManager(this).token?.let {
            mPresenter.getAll("Bearer $it")
        }
    }

    private fun setRecyclerView() {
        rv_main_history.adapter = adapter
        rv_main_history.layoutManager = LinearLayoutManager(this)
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun onResume() {
        PreferenceManager(this).token?.let {
            mPresenter.getAll("Bearer $it")
        }
        super.onResume()
    }
}
