package untag.f4_1461600225daskom.fitcare.ui.orderconfirmation

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.squareup.okhttp.ResponseBody
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import com.wdullaer.materialdatetimepicker.time.Timepoint
import kotlinx.android.synthetic.main.activity_order_confirmation.*
import retrofit2.Converter
import retrofit2.HttpException
import retrofit2.adapter.rxjava.Result.response
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.Order
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.ui.GetTherapistActivity
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager
import java.text.SimpleDateFormat
import java.util.*


class OrderConfirmationActivity : BaseMvpActivity<OrderConfirmationContract.View, OrderConfirmationContract.Presenter>(), OrderConfirmationContract.View,
    TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    private var dateOrdered=""
    private val dialog: Dialog by lazy { Dialog(this) }
    override var mPresenter: OrderConfirmationContract.Presenter = OrderConfirmationPresenter()
    var date = ""
    var time = ""

    override fun showResponseMessage(response: Order.OrderResponse) {
        dialog.dismiss()
        if (response.meta.code == 200) {
            Toast.makeText(this, response.meta.message, Toast.LENGTH_SHORT).show()
            val intent = Intent(this, GetTherapistActivity::class.java).also {
                if (response.data.mitra.user_data.display_picture != null)
                    it.putExtra("therapist_image", response.data.mitra.user_data.display_picture)
                it.putExtra("therapist_name", response.data.mitra.user_data.full_name)
                it.putExtra("invoice", response.data.invoice.invoice_code)
                it.putExtra("location", response.data.mitra.user_data.address)
                it.putExtra("estimation", "")
                it.putExtra("total_cost", response.data.invoice.details.total.toString())
            }
            startActivity(intent)
        }else {
            Toast.makeText(this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show()
        }
    }

    override fun hideProgress() {
        dialog.dismiss()
    }

    override fun showResponseErrorMessage(it: Throwable) {
        val error: HttpException = it as HttpException
        val errorBody: String = error.message().toString()
        if (errorBody.toLowerCase().equals("forbidden")){
            Toast.makeText(this, "Mitra Tidak Ditemukan, jarak mitra terlalu jauh", Toast.LENGTH_SHORT).show()
        }else if (errorBody.toLowerCase().equals("not found")) {
            Toast.makeText(this, "Mitra Tidak Ditemukan", Toast.LENGTH_SHORT).show()
        }else if (errorBody.toLowerCase().equals("payment required")) {
            Toast.makeText(this, "Saldo tidak mencukupi atau minimal saldo yang tersisa setelah transaksi adalah Rp30000", Toast.LENGTH_SHORT).show()
        }

        Toast.makeText(this, "Silahkan ulangi beberapa saat lagi", Toast.LENGTH_LONG).show()
//        val jsonObject = JSONObject(error.response()?.body().toString())
//        val meta: String = jsonObject.getString("meta")
//        Toast.makeText(this, meta, Toast.LENGTH_SHORT).show()
        Log.d("error", errorBody)



    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_confirmation)

        setToolbar()

        tv_tgl.text = "Pilih tanggal booking"
        cv_tanggal.setOnClickListener {
            val now = Calendar.getInstance()
            val dpd =
                DatePickerDialog.newInstance(
                    this,
                    now[Calendar.YEAR],  // Initial year selection
                    now[Calendar.MONTH],  // Initial month selection
                    now[Calendar.DAY_OF_MONTH] // Inital day selection
                )
// If you're calling this from a support Fragment
// If you're calling this from a support Fragment


            dpd.minDate = now
            dpd.show(getSupportFragmentManager(), "Datepickerdialog")
        }

        Log.d("service id", intent.getIntExtra("service_id", 1).toString())

        tv_alamat.text= intent.getStringExtra("address")
        tv_durasi_pijet.text = "Durasi ${intent.getStringExtra("menit")} menit"
        tv_harga_pijet.text = "Rp. ${intent.getStringExtra("price")}"
        tv_total_harga_pijet.text = "Rp. ${intent.getStringExtra("price")}"

        //check service
        if (intent.getIntExtra("service_id", 1).toString().equals("1")){
            tv_jenis_terapist.text = "Baby Spa"
        }else if (intent.getIntExtra("service_id", 1).toString().equals("2")){
            tv_jenis_terapist.text = "Mom Spa"
        }else if (intent.getIntExtra("service_id", 1).toString().equals("3")){
            tv_jenis_terapist.text = "Kids Spa"
        }else{
            tv_jenis_terapist.text = "Dad Spa"
        }

//        //current date
//        val localeId = Locale("in", "ID")
//        val currentDate = Calendar.getInstance()
//        tv_tgl.text = SimpleDateFormat("EEEE, dd MMM kk:mm", localeId).format(currentDate.time)

        cl_alamat.setOnClickListener {
            finish()
        }

        cardview_container_send.setOnClickListener {
            if (!tv_tgl.text.toString().equals("Pilih tanggal booking")){
                val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)

                // set pesan dari dialog
                alertDialogBuilder
                    .setMessage("Apakah Anda ingin melakukan order!")
                    .setIcon(R.mipmap.ic_launcher)
                    .setCancelable(false)
                    .setPositiveButton(
                        "Ya",
                        DialogInterface.OnClickListener { dialog, id -> // jika tombol diklik, maka akan menutup activity ini
                            PreferenceManager(this).token?.let {
                                showProgress()
                                mPresenter.submitOrder(
                                    "Bearer $it",
                                    intent.getIntExtra("service_id", 1).toString(),
                                    intent.getStringExtra("longitude"),
                                    intent.getStringExtra("latitude"),
                                    dateOrdered,
                                    et_note.text.toString(),
                                    intent.getStringExtra("menit"),
                                    "cashless"
                                )
                            }
                        })
                    .setNegativeButton(
                        "Tidak",
                        DialogInterface.OnClickListener { dialog, id -> // jika tombol ini diklik, akan menutup dialog
                            // dan tidak terjadi apa2
                            dialog.cancel()
                        })

                // membuat alert dialog dari builder

                // membuat alert dialog dari builder
                val alertDialog: AlertDialog = alertDialogBuilder.create()

                // menampilkan alert dialog

                // menampilkan alert dialog
                alertDialog.show()

            }else{
                Toast.makeText(this,"Isi Waktu Pemesanan Terlebih Dahulu",Toast.LENGTH_LONG).show()
            }

        }
    }

    private fun showProgress() {
        dialog.setContentView(R.layout.dialog_progress_order)
        dialog.setCancelable(false)
        dialog.show()
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun onTimeSet(view: TimePickerDialog?, hourOfDay: Int, minute: Int, second: Int) {
        time =
            hourOfDay.toString() + ":" + minute.toString() + ":" + second

        dateOrdered = date+" "+time

        val parser =  SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val formatter = SimpleDateFormat("EEEE, dd MMM kk:mm")
        val formattedDate = formatter.format(parser.parse(dateOrdered))


        tv_tgl.text = formattedDate
    }

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        date =
            year.toString() + "-" + toDate((monthOfYear + 1).toString()) + "-" + toDate(dayOfMonth.toString())

        if (date.isNotEmpty()){
            val now = Calendar.getInstance()
            val tpd = TimePickerDialog.newInstance(
                this,
                now[Calendar.HOUR_OF_DAY],
                now[Calendar.MINUTE],
                true
            )

            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val currentDate = sdf.format(Date())

            val currentTime =
                SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date()).split(":")

            if (date.equals(currentDate)) {
                val timePoint = Timepoint(
                    currentTime[0].toInt() + 1,
                    currentTime[1].toInt(),
                    currentTime[2].toInt()
                )
                tpd.setMinTime(timePoint)

            }
            tpd.show(getSupportFragmentManager(), "Timepickerdialog")
        }
    }

    fun toDate(a:String): String{
        var time : String = ""

        if (a.toInt() <10 && a.toInt() > 0){
            time = "0${a}"
        }else{
            time = a
        }
        return time
    }

}
