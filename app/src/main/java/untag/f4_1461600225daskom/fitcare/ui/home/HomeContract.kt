package untag.f4_1461600225daskom.fitcare.ui.home

import untag.f4_1461600225daskom.fitcare.model.Home
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface HomeContract{
    interface View: BaseMvpView {
        fun showResponseMessage(response: Home.HomeResponse)

        fun hideProgress()
    }

    interface Presenter: BaseMvpPresenter<View> {
        fun getAll(token:String)
    }
}