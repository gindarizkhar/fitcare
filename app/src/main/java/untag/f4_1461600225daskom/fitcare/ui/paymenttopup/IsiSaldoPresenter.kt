package untag.f4_1461600225daskom.fitcare.ui.paymenttopup

import okhttp3.MultipartBody
import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class IsiSaldoPresenter : BaseMvpPresenterImpl<IsiSaldoContract.View>(), IsiSaldoContract.Presenter {

    override fun getProfile(token: String) {
        ApiManager.getProfile(token)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showResponse(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }

    override fun insertMidtrans(token: String, nominal: String, bank: String) {
        ApiManager.insertMidtrans(token,nominal,bank)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showResponseMidtrans(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }


}