package untag.f4_1461600225daskom.fitcare.ui.register

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_register.*
import untag.f4_1461600225daskom.fitcare.ui.otp.OtpActivity
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.Register
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.ui.login.LoginActivity

class RegisterActivity : BaseMvpActivity<RegisterContract.View,RegisterContract.Presenter>(),RegisterContract.View {
    override var mPresenter: RegisterContract.Presenter=RegisterPresenter()

    private var phonenumber=""

    override fun showResponseMessage(response: Register.RegisterResponse) {
        pg_register.visibility = View.INVISIBLE
        btnregister.visibility = View.VISIBLE
        if(response.meta.code==200) {
            val intent = Intent(this, OtpActivity::class.java)
            intent.putExtra("from", "register")
            intent.putExtra("phone", nohp.text.toString())
            startActivity(intent)
        }
    }
    override fun hideProgress() {
        pg_register.visibility = View.INVISIBLE
        btnregister.visibility = View.VISIBLE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)


        phonenumber = nohp.text.toString()
        btnregister.setOnClickListener {
            if (nohp.getText().toString() == "") {
                Toast.makeText(applicationContext, "Isikan Nomer Hp", Toast.LENGTH_SHORT).show()
            } else if (email.getText().toString() == "") {
                Toast.makeText(applicationContext, "Isikan Alamat Email", Toast.LENGTH_SHORT).show()
            } else if (namalengkap.getText().toString() == "") {
                Toast.makeText(applicationContext, "Isikan Nama Lengkap", Toast.LENGTH_SHORT).show()
            }else{
                pg_register.visibility = View.VISIBLE
                btnregister.visibility = View.INVISIBLE
                addData()
            }
        }

        tv_login.setOnClickListener(View.OnClickListener {
            val Intent = Intent(this, LoginActivity::class.java)
            startActivity(Intent)
        })
    }

    private fun addData() {
        mPresenter.submitregister(nohp.text.toString(),email.text.toString(),namalengkap.text.toString())
    }
}
