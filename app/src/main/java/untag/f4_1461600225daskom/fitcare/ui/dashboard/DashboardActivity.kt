package untag.f4_1461600225daskom.fitcare.ui.dashboard

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.activity_order_confirmation.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.Dashboard
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.ui.history.HistoryActivity
import untag.f4_1461600225daskom.fitcare.ui.home.HomeFragment
import untag.f4_1461600225daskom.fitcare.ui.mitra_register.PendaftaranMitraUploadContract
import untag.f4_1461600225daskom.fitcare.ui.mitra_register.PendaftaranMitraUploadPresenter
import untag.f4_1461600225daskom.fitcare.ui.profile.ProfileFragment
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager

class DashboardActivity : BaseMvpActivity<DashboardContract.View, DashboardContract.Presenter>(), DashboardContract.View  {

    override var mPresenter: DashboardContract.Presenter = DashboardPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        initBottomNav()
        runFCM()
    }

    private fun runFCM() {
        try {
            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w(
                            "firebase",
                            "getInstanceId failed",
                            task.exception
                        )
                        return@OnCompleteListener
                    }

                    // Get new Instance ID token
                    val token = task.result!!.token

                    //untuk save token ke api
                    PreferenceManager(this).token?.let {
                        mPresenter.submitFCM(
                            "Bearer $it",
                            token
                        )
                    }
                    Log.d("Token: ", token)
                    Log.d("firebase", "true")
                    FirebaseMessaging.getInstance()
                        .subscribeToTopic("untag.f4_1461600225daskom.fitcare.schedule")
                })
        } catch (e: Exception) {
        }
    }

    override fun onResume() {
        super.onResume()
        nav_view.currentItem = bottomNavPosition
    }

    private var bottomNavPosition = 0
    private fun initBottomNav() {
        val item1 = AHBottomNavigationItem("Home", R.drawable.icons8_home_filled_100px_1)
        val item2 = AHBottomNavigationItem("History", R.drawable.icons8_message_filled_100px)
        val item3 = AHBottomNavigationItem("Profile", R.drawable.icons8_user_filled_100px)

        nav_view.addItem(item1)
        nav_view.addItem(item2)
        nav_view.addItem(item3)

        nav_view.defaultBackgroundColor = Color.parseColor("#FFFFFF")
        nav_view.accentColor = ContextCompat.getColor(this, R.color.black)
        nav_view.inactiveColor = ContextCompat.getColor(this, R.color.grey)
        nav_view.titleState = AHBottomNavigation.TitleState.ALWAYS_SHOW

        nav_view.setOnTabSelectedListener { position, _ ->
            var bottomNavFragment: androidx.fragment.app.Fragment? = null
            var isFragment = false
            when (position) {
                0 -> {
                    bottomNavFragment = HomeFragment()
                    isFragment = true
                    bottomNavPosition = 0
                }
                1 -> {
                    startActivity(Intent(this, HistoryActivity::class.java))
                    nav_view.isSelected = false
                }
                2 -> {
                    bottomNavFragment = ProfileFragment()
                    isFragment = true
                    bottomNavPosition = 2
                }
            }
            if (isFragment) {
                bottomNavFragment?.let {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.fl_container, it)
                        .commitAllowingStateLoss()
                }
            }
            true
        }

        nav_view.currentItem = 0
    }

    override fun showResponseMessage(response: Dashboard.DashboardResponse) {

    }

    override fun hideProgress() {
    }

    override fun hideProgressMessage(it: Throwable?) {

    }
}
