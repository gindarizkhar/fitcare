package untag.f4_1461600225daskom.fitcare.ui.belumsertifikasi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_belum_sertifikasi.*
import kotlinx.android.synthetic.main.activity_belum_sertifikasi.toolbar
import kotlinx.android.synthetic.main.activity_belum_sertifikasi_detail.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.BelumSertifikasi
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.ui.mitra_register.PendaftaranMitraActivity
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager

class BelumSertifikasiDetailActivity : BaseMvpActivity<BelumSertifikasiContract.View, BelumSertifikasiContract.Presenter>(), BelumSertifikasiContract.View {

    override var mPresenter: BelumSertifikasiContract.Presenter = BelumSertifikasiPresenter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_belum_sertifikasi_detail)

        setToolbar()

        PreferenceManager(this).token?.let {
            mPresenter.getSertifikasiDetail(
                "Bearer $it",
                intent.getStringExtra("id")
            )
        }
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun showResponseMessage(response: BelumSertifikasi.BelumSertifikasiResponse) {
        TODO("Not yet implemented")
    }

    override fun showResponseMessageDetail(response: BelumSertifikasi.BelumSertifikasiDetailResponse) {
        tv_layanan.text = response.data.schedule.service.service
        tv_kode_schedule.text = response.data.schedule.schedule_code
        tv_waktu.text = "Tanggal Registrasi : ${response.data.schedule.date_regist}\nTanggal Registrasi Berakhir: ${response.data.schedule.date_regist_expired}\nTanggal Sertifikasi : ${response.data.schedule.date_certificate}\nTanggal Sertifikasi Berakhir : ${response.data.schedule.date_certificate_expired}"
        tv_kuota.text = "Kuota : ${response.data.schedule.participant_max}\nKuota terisi : ${response.data.schedule.participant_regist}"
        tv_info.text = response.data.schedule.content
        tv_harga.text = "Rp${response.data.schedule.price}"

        cv_daftar.setOnClickListener {
            val sudah = Intent(this,
                PendaftaranMitraActivity::class.java)
            sudah.putExtra("schedule_id",response.data.schedule.id.toString())
            sudah.putExtra("status","0")
            startActivity(sudah)
        }
    }

    override fun hideProgress() {
        TODO("Not yet implemented")
    }

    override fun hideProgressMessage(it: Throwable?) {
        TODO("Not yet implemented")
    }
}