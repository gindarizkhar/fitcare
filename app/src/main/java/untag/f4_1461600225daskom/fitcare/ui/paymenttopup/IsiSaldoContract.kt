package untag.f4_1461600225daskom.fitcare.ui.paymenttopup


import okhttp3.MultipartBody
import untag.f4_1461600225daskom.fitcare.model.Profile
import untag.f4_1461600225daskom.fitcare.model.TopUp
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface IsiSaldoContract {
    interface View: BaseMvpView {
        fun showResponse(response: Profile.ProfileResponse)

        fun showResponseMidtrans(response: TopUp.TopUpResponse)

        fun hideProgress()

        fun hideProgressUpload()
    }
    interface Presenter: BaseMvpPresenter<View>{
        fun getProfile(token: String)

        fun insertMidtrans(token: String, nominal:String, bank: String)
    }
}