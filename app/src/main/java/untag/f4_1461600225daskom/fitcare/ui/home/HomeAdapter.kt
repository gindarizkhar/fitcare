package untag.f4_1461600225daskom.fitcare.ui.home

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_featured_home.view.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.Home

class HomeAdapter : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    private lateinit var context: Context
    private val features: MutableList<Home.News> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_featured_home, parent, false))
    }

    override fun getItemCount(): Int=features.size

    fun addAll(news: MutableList<Home.News>)
    {
        features.clear()
        features.addAll(news)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binditem(features[position], context)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
    {
        fun binditem(news: Home.News, context: Context){
            itemView.txt_title.text = news.title.trim()
            itemView.txt_description.text = news.content.trim()
            Picasso.get().load(news.image).into(itemView.img_photo)

            itemView.setOnClickListener {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(news.link_news))
                context.startActivity(browserIntent)
            }
        }
    }
}