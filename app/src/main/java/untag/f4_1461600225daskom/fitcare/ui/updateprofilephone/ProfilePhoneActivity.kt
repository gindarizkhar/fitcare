package untag.f4_1461600225daskom.fitcare.ui.updateprofilephone

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit_profile.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.Profile
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.ui.otp.OtpActivity
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager

class ProfilePhoneActivity : BaseMvpActivity<ProfilePhoneContract.View, ProfilePhoneContract.Presenter>(), ProfilePhoneContract.View {

    override var mPresenter: ProfilePhoneContract.Presenter= ProfilePhonePresenter()

    override fun showResponse(response: Profile.UpdatePhoneResponse) {
        val intent = Intent(this, OtpActivity::class.java)
        intent.putExtra("from", "profile")
        intent.putExtra("phone", response.data.phone_new)
        startActivity(intent)
        finish()
    }

    override fun hideProgress() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        initView()

        button_save.setOnClickListener {
            PreferenceManager(this).token?.let {
                mPresenter.submitPhone("Bearer $it", et_input.text.toString())
            }
        }
    }

    private fun initView() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
        toolbar_title.text = "Edit Phone"
        ti_input.hint = "Input No. Telp"
        et_input.setText(intent.getStringExtra("phone"))
    }
}
