package untag.f4_1461600225daskom.fitcare.ui.dashboard

import okhttp3.MultipartBody
import okhttp3.RequestBody
import untag.f4_1461600225daskom.fitcare.model.Dashboard
import untag.f4_1461600225daskom.fitcare.model.RegisMitra
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface DashboardContract {
    interface View: BaseMvpView {
        fun showResponseMessage(response: Dashboard.DashboardResponse)

        fun hideProgress()
        fun hideProgressMessage(it: Throwable?)
    }
    interface Presenter: BaseMvpPresenter<View> {
        fun submitFCM(authorization: String,
                              token: String

        )
    }
}