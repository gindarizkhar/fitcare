package untag.f4_1461600225daskom.fitcare.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_jadi_mitra.*
import kotlinx.android.synthetic.main.terms_and_conditions.toolbar
import untag.f4_1461600225daskom.fitcare.R

class JadiMitraActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jadi_mitra)

        setToolbar()

        iv_mom.setOnClickListener {
            val term = Intent(this,TermJadiMitraActivity::class.java)
            term.putExtra("service_id","2")
            startActivity(term)
        }

        iv_baby.setOnClickListener {
            val term = Intent(this,TermJadiMitraActivity::class.java)
            term.putExtra("service_id","1")
            startActivity(term)
        }

        iv_kids.setOnClickListener {
            val term = Intent(this,TermJadiMitraActivity::class.java)
            term.putExtra("service_id","3")
            startActivity(term)
        }

        iv_dad.setOnClickListener {
            val term = Intent(this,TermJadiMitraActivity::class.java)
            term.putExtra("service_id","4")
            startActivity(term)
        }
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
    }
}