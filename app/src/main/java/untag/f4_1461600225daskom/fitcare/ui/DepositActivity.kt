package untag.f4_1461600225daskom.fitcare.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_deposit.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.ui.paymenttopup.IsiSaldoActivity
import untag.f4_1461600225daskom.fitcare.ui.paymenttopup_history.TopupHistoryActivity

class DepositActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deposit)

        jumlahsaldo.text = "Rp${intent.getStringExtra("amount")}"
        isisaldo.setOnClickListener {
            val isisaldo = Intent(this, IsiSaldoActivity::class.java)
            startActivity(isisaldo)
        }

        riwayat.setOnClickListener {
            val riwayat = Intent(this, TopupHistoryActivity::class.java)
            startActivity(riwayat)
        }
    }
}
