package untag.f4_1461600225daskom.fitcare.ui.belumsertifikasi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.frogobox.recycler.boilerplate.viewrclass.FrogoViewAdapterCallback
import kotlinx.android.synthetic.main.activity_belum_sertifikasi.*
import kotlinx.android.synthetic.main.activity_history_detail2.*
import kotlinx.android.synthetic.main.item_history.view.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.BelumSertifikasi
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.ui.historydetail.HistoryDetailContract
import untag.f4_1461600225daskom.fitcare.ui.historydetail.HistoryDetailPresenter
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager
import untag.f4_1461600225daskom.fitcare.utils.customFormat

class belum_sertifikasi : BaseMvpActivity<BelumSertifikasiContract.View, BelumSertifikasiContract.Presenter>(), BelumSertifikasiContract.View {

    override var mPresenter: BelumSertifikasiContract.Presenter = BelumSertifikasiPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_belum_sertifikasi)

        setToolbar()

        PreferenceManager(this).token?.let {
            mPresenter.getSertifikasi(
                "Bearer $it"
            )
        }
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun showResponseMessage(response: BelumSertifikasi.BelumSertifikasiResponse) {

        if (response.meta.code == 200){
            val adapterCallback = object :
                FrogoViewAdapterCallback<BelumSertifikasi.Schedule> {
                override fun setupInitComponent(view: View, data: BelumSertifikasi.Schedule) {
                        when (data.service_id) {
                            "1" -> {
                                view.findViewById<TextView>(R.id.tv_history_service).text =
                                    "Baby Spa"
                                Glide.with(this@belum_sertifikasi)
                                    .load(R.drawable.icons_babys_room_white_60px)
                                    .into(view.findViewById<ImageView>(R.id.iv_history_icon))
                            }
                            "2" -> {
                                view.findViewById<TextView>(R.id.tv_history_service).text =
                                    "Mom Spa"
                                Glide.with(this@belum_sertifikasi)
                                    .load(R.drawable.icons_babys_spa_white_60px)
                                    .into(view.findViewById<ImageView>(R.id.iv_history_icon))
                            }
                            "3" -> {
                                view.findViewById<TextView>(R.id.tv_history_service).text =
                                    "Kids Spa"
                                Glide.with(this@belum_sertifikasi)
                                    .load(R.drawable.kids_b)
                                    .into(view.findViewById<ImageView>(R.id.iv_history_icon))
                            }
                            "4" -> {
                                view.findViewById<TextView>(R.id.tv_history_service).text =
                                    "Dad Spa"
                                Glide.with(this@belum_sertifikasi)
                                    .load(R.drawable.dad_b)
                                    .into(view.findViewById<ImageView>(R.id.iv_history_icon))
                            }
                            else -> {
                            }
                        }

                        val orderDate = data.date_regist.customFormat("yyyy-MM-dd", "EEE, dd MMM")
                        view.findViewById<TextView>(R.id.tv_history_date).text = orderDate
                        view.findViewById<TextView>(R.id.tv_history_state).text =
                            "Kuota : ${data.participant_max}"
                }

                override fun onItemClicked(data: BelumSertifikasi.Schedule) {
                    val detail = Intent(this@belum_sertifikasi, BelumSertifikasiDetailActivity::class.java)
                    detail.putExtra("id",data.id.toString())
                    startActivity(detail)
                }

                override fun onItemLongClicked(data: BelumSertifikasi.Schedule) {
                    // setup item long clicked on frogo recycler view
                }
            }

            recycler_view
                .injector<BelumSertifikasi.Schedule>()
                .addData(response.data.schedule)
                .addCustomView(R.layout.item_history)
                .addEmptyView(null)
                .addCallback(adapterCallback)
                .createLayoutLinearVertical(false)
                .build()
        }
    }

    override fun showResponseMessageDetail(response: BelumSertifikasi.BelumSertifikasiDetailResponse) {

    }

    override fun hideProgress() {
        TODO("Not yet implemented")
    }

    override fun hideProgressMessage(it: Throwable?) {
        TODO("Not yet implemented")
    }
}