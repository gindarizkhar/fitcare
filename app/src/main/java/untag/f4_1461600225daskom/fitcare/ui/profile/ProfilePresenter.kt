package untag.f4_1461600225daskom.fitcare.ui.profile

import okhttp3.MultipartBody
import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class ProfilePresenter : BaseMvpPresenterImpl<ProfileContract.View>(), ProfileContract.Presenter {

    override fun getProfile(token: String) {
        ApiManager.getProfile(token)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showResponse(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }

    override fun uploadProfileImage(token: String, image: MultipartBody.Part) {
        ApiManager.uploadProfileImage(token, image)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showUploadResponse(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgressUpload()}
            )
    }

    override fun verifyEmail(token: String) {
        ApiManager.verifyEmail(token)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showVerifyResponse(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> }
            )
    }

}