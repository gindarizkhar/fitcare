package untag.f4_1461600225daskom.fitcare.ui.profile


import okhttp3.MultipartBody
import untag.f4_1461600225daskom.fitcare.model.Profile
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface ProfileContract {
    interface View: BaseMvpView {
        fun showResponse(response: Profile.ProfileResponse)

        fun showUploadResponse(response: Profile.UploadProfileImageResponse)

        fun showVerifyResponse(response: Profile.VerifyEmailResponse)

        fun hideProgress()

        fun hideProgressUpload()
    }
    interface Presenter: BaseMvpPresenter<View>{
        fun getProfile(token: String)

        fun uploadProfileImage(token: String, image: MultipartBody.Part)

        fun verifyEmail(token: String)
    }
}