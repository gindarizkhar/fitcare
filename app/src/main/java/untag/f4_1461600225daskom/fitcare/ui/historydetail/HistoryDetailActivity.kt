package untag.f4_1461600225daskom.fitcare.ui.historydetail

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_get_therapist.*
import kotlinx.android.synthetic.main.activity_history_detail2.*
import kotlinx.android.synthetic.main.activity_history_detail2.iv_therapist
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import untag.f4_1461600225daskom.fitcare.BuildConfig.BASE_URL_IMAGE
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.HistoryDetail
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.ui.dashboard.DashboardContract
import untag.f4_1461600225daskom.fitcare.ui.dashboard.DashboardPresenter
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager
import untag.f4_1461600225daskom.fitcare.utils.formatCurrency
import untag.f4_1461600225daskom.fitcare.utils.localeId

class HistoryDetailActivity : BaseMvpActivity<HistoryDetailContract.View, HistoryDetailContract.Presenter>(), HistoryDetailContract.View {

    override var mPresenter: HistoryDetailContract.Presenter = HistoryDetailPresenter()

    @ExperimentalStdlibApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_detail2)

        setToolbar()

        PreferenceManager(this).token?.let {
            mPresenter.getDetail(
                "Bearer $it",
                intent.getStringExtra("id")
            )
        }


    }

    private fun setToolbar() {
        toolbar_detail_history.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar_detail_history.setNavigationOnClickListener { finish() }
    }

    override fun showResponseMessage(response: HistoryDetail.HistoryResponse) {
        Toast.makeText(applicationContext, "Berhasil", Toast.LENGTH_SHORT).show()
        PreferenceManager(this).token?.let {
            mPresenter.getDetail(
                "Bearer $it",
                intent.getStringExtra("id")
            )
        }
    }

    override fun showResponseMessageDetail(response: HistoryDetail.HistoryResponse) {
        if (response.meta.code == 200){
            tv_history_detail_invoice.text = response.data.order.invoice.invoice_code
            tv_history_detail_name.text = response.data.order.invoice.mitra_details.user_data.full_name
            tv_history_detail_location.text = response.data.order.invoice.details.loc_mitra_now

            var timetrip = ""
            if (response.data.order.invoice.details.time_trip != null)
                timetrip ="(Est. ${response.data.order.invoice.details.time_trip})"
            else
                timetrip =""
            tv_history_detail_time.text = "${response.data.order.time}\t ${timetrip}"
            tv_history_detail_cost.text = "Rp ${response.data.order.invoice.details.total.toFloat().formatCurrency()}"

            var image = response.data.order.invoice.mitra_details.user_data.display_picture

            if (image != null)
                Glide.with(this).load(BASE_URL_IMAGE+image).into(iv_therapist)
            else
                Glide.with(this).load(R.drawable.ic_person_black_24dp).into(iv_therapist)

            if (response.data.order.invoice.status.toLowerCase().equals("on the way")){
                cv_selesai.visibility = View.VISIBLE
                cv_selesai.setOnClickListener {
                    val alertDialogBuilder =
                        AlertDialog.Builder(
                            this
                        )

                    // set title dialog

                    // set title dialog
                    alertDialogBuilder.setTitle("Selesaikan transaksi?")

                    // set pesan dari dialog

                    // set pesan dari dialog
                    alertDialogBuilder
                        .setTitle("Konfirmasi order selesai")
                        .setMessage("Apakah Anda ingin mengkonfirmasi bahwa order sudah selesai?")
                        .setIcon(R.mipmap.ic_launcher)
                        .setCancelable(false)
                        .setPositiveButton(
                            "Ya"
                        ) { dialog, id ->
                            PreferenceManager(this).token?.let {
                                mPresenter.submitSelesai(
                                    "Bearer $it",
                                    intent.getStringExtra("id")
                                )
                            }
                        }
                        .setNegativeButton(
                            "Tidak"
                        ) { dialog, id -> // jika tombol ini diklik, akan menutup dialog
                            // dan tidak terjadi apa2
                            dialog.cancel()
                        }

                    // membuat alert dialog dari builder

                    // membuat alert dialog dari builder
                    val alertDialog = alertDialogBuilder.create()

                    // menampilkan alert dialog

                    // menampilkan alert dialog
                    alertDialog.show()
                }
            }else{
                cv_selesai.visibility = View.GONE
            }
        }
    }

    override fun hideProgress() {
        TODO("Not yet implemented")
    }

    override fun hideProgressMessage(it: Throwable?) {
        TODO("Not yet implemented")
    }

    override fun onResume() {
        PreferenceManager(this).token?.let {
            mPresenter.getDetail(
                "Bearer $it",
                intent.getStringExtra("id")
            )
        }
        super.onResume()
    }
}
