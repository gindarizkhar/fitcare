package untag.f4_1461600225daskom.fitcare.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_get_therapist.*
import untag.f4_1461600225daskom.fitcare.BuildConfig.BASE_URL
import untag.f4_1461600225daskom.fitcare.BuildConfig.BASE_URL_IMAGE
import untag.f4_1461600225daskom.fitcare.PaymentActivity
import untag.f4_1461600225daskom.fitcare.R

class GetTherapistActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_therapist)

        if (intent.hasExtra("therapist_image"))
            Glide.with(this).load(BASE_URL_IMAGE+intent.getStringExtra("therapist_image")).into(iv_therapist)
        else
            Glide.with(this).load(R.drawable.ic_person_black_24dp).into(iv_therapist)
        tv_therapist_name.text = intent.getStringExtra("therapist_name")

        button_next.setOnClickListener {
            val intent = Intent(this, PaymentActivity::class.java).also {
                it.putExtra("therapist_name", intent.getStringExtra("therapist_name"))
                it.putExtra("invoice", intent.getStringExtra("invoice"))
                it.putExtra("location", intent.getStringExtra("location"))
                it.putExtra("estimation", intent.getStringExtra("estimation"))
                it.putExtra("total_cost", intent.getStringExtra("total_cost"))
            }
            startActivity(intent)
        }
    }
}
