package untag.f4_1461600225daskom.fitcare.ui.updateprofileaddress

import untag.f4_1461600225daskom.fitcare.model.Profile
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface ProfileAddressContract {
    interface View: BaseMvpView {
        fun showResponse(response: Profile.UpdateAddressResponse)

        fun hideProgress()
    }
    interface Presenter: BaseMvpPresenter<View> {
        fun submitAddress(token: String, address: String)
    }
}