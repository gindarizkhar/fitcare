package untag.f4_1461600225daskom.fitcare.ui.updateprofilename

import untag.f4_1461600225daskom.fitcare.model.Profile
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface ProfileNameContract {
    interface View: BaseMvpView {
        fun showResponse(response: Profile.UpdateNameResponse)

        fun hideProgress()
    }
    interface Presenter: BaseMvpPresenter<View> {
        fun submitName(token: String, fullName: String)
    }
}