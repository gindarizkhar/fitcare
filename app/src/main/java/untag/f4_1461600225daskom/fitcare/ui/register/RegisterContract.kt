package untag.f4_1461600225daskom.fitcare.ui.register


import untag.f4_1461600225daskom.fitcare.model.Register
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface RegisterContract {
    interface View: BaseMvpView {
        fun showResponseMessage(response: Register.RegisterResponse)

        fun hideProgress()
    }

    interface Presenter: BaseMvpPresenter<View> {
        fun submitregister(email:String,phone:String,full_name:String)
    }
}