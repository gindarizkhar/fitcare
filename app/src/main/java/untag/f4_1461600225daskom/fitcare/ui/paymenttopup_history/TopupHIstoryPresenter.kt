package untag.f4_1461600225daskom.fitcare.ui.paymenttopup_history

import okhttp3.MultipartBody
import rx.functions.Action1
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenterImpl
import untag.f4_1461600225daskom.fitcare.network.ApiManager
import untag.f4_1461600225daskom.fitcare.network.ErrorResponseHandler

class TopupHIstoryPresenter : BaseMvpPresenterImpl<TopupHistoryContract.View>(), TopupHistoryContract.Presenter {

    override fun getTopupHistory(token: String) {
        ApiManager.getTopupHistory(token)
            .doOnError{mView?.showMessage(it.toString())}
            .subscribe(
                Action1 { mView?.showResponse(it) },
                ErrorResponseHandler(mView,true){_,_,_ -> mView?.hideProgress()}
            )
    }


}