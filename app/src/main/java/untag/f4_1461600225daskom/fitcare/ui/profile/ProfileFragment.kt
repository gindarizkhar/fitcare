package untag.f4_1461600225daskom.fitcare.ui.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.Toast
import com.github.dhaval2404.imagepicker.ImagePicker
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_update_profile.view.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.Profile
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpFragment
import untag.f4_1461600225daskom.fitcare.ui.mitra_register.PendaftaranMitraUploadActivity
import untag.f4_1461600225daskom.fitcare.ui.login.LoginActivity
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager
import java.io.File
import java.io.IOException

class ProfileFragment : BaseMvpFragment<ProfileContract.View, ProfileContract.Presenter>(), ProfileContract.View {

    private lateinit var root: View

    private lateinit var transactionImage: File



    override var mPresenter: ProfileContract.Presenter = ProfilePresenter()

    override fun showResponse(response: Profile.ProfileResponse) {
        if (response.meta.code == 200) {
            root.tv_nama_profile.text = response.data.user.user_data.full_name
            root.tv_email_profile.text = response.data.user.email
            root.tv_telefon_profile.text = response.data.user.phone
            root.tv_alamat_profile.text = response.data.user.user_data.address
            Picasso.get().load(response.data.user.user_data.display_picture)
                .into(root.profile_image)


            root.profile_image.setOnClickListener {

                ImagePicker.with(this)
                    .compress(1024)
                    .maxResultSize(720, 720)
                    .saveDir(
                        File(
                            Environment.getExternalStorageDirectory(),
                            "Fit Care"
                        )
                    )
                    .start { resultCode, data ->
                        if (resultCode == Activity.RESULT_OK) {
                            //You can also get File Path from intent
                            val imageUri: String? = ImagePicker.getFilePath(data)
                            Log.e("lokasi image ", imageUri)
                            try {
                                @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
                                transactionImage = File(imageUri)
                                val fileReqBody =
                                    transactionImage.asRequestBody("image/*".toMediaTypeOrNull())
                                val image = MultipartBody.Part.createFormData(
                                    "file",
                                    transactionImage.name,
                                    fileReqBody
                                )
                                PreferenceManager(context).token?.let {
                                    progress_upload_profile_image.visibility = View.VISIBLE
                                    profile_image.visibility = View.INVISIBLE
                                    mPresenter.uploadProfileImage("Bearer $it", image)
                                }
                            } catch (e: IOException) {
                                Log.e("choose image error ", e.toString())
                            }

                        }
                    }
            }

            root.btn_edit_profile.setOnClickListener {
                val btnEditProfile = Intent(context, UpdateProfileActivity::class.java)
                btnEditProfile.putExtra("full_name", response.data.user.user_data.full_name)
                btnEditProfile.putExtra("email", response.data.user.email)
                btnEditProfile.putExtra("phone", response.data.user.phone)
                btnEditProfile.putExtra("address", response.data.user.user_data.address)
                startActivity(btnEditProfile)
            }

            root.cardview5.setOnClickListener {
                PreferenceManager(context).clearData()
                val logout = Intent(context, LoginActivity::class.java)
                startActivity(logout)
            }
        }else if (response.meta.code == 401){
            PreferenceManager(context).clearData()
            val logout = Intent(context, LoginActivity::class.java)
            startActivity(logout)
        }
    }

    override fun showUploadResponse(response: Profile.UploadProfileImageResponse) {
        Log.d("image response", "foto berhasil dirubah")
        progress_upload_profile_image.visibility = View.GONE
        profile_image.visibility = View.VISIBLE

        PreferenceManager(context).token?.let {
            mPresenter.getProfile("Bearer $it")
        }
        Toast.makeText(context, "Foto Profil Berhasil Diubah", Toast.LENGTH_SHORT).show()
    }

    override fun showVerifyResponse(response: Profile.VerifyEmailResponse) {
    }

    override fun hideProgress() {
        progress_upload_profile_image.visibility = View.GONE
        profile_image.visibility = View.VISIBLE
    }

    override fun hideProgressUpload() {
    }

    override fun showMessageErrorLogin() {
    }

    override fun showMessageOTP() {
        TODO("Not yet implemented")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        root = inflater.inflate(R.layout.fragment_profile, container, false)

        PreferenceManager(context).token?.let {
            mPresenter.getProfile("Bearer $it")
        }

        root.profile_verivikasi_akun.setOnClickListener{
                val verivikasiakun = Intent(context,
                    PendaftaranMitraUploadActivity::class.java)
                verivikasiakun.putExtra("status","2")
                startActivity(verivikasiakun)
        }

        return root
    }

}