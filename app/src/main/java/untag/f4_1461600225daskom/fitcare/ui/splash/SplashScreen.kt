package untag.f4_1461600225daskom.fitcare.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.OtpResponse
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.ui.dashboard.DashboardActivity
import untag.f4_1461600225daskom.fitcare.ui.login.LoginActivity

class SplashScreen : BaseMvpActivity<SplashContract.View, SplashContract.Presenter>(), SplashContract.View {

    override var mPresenter: SplashContract.Presenter = SplashPresenter()
    private val prefManager: PreferenceManager by lazy {
        PreferenceManager(
            this
        )
    }

    override fun showResponse(response: OtpResponse) {
        prefManager.token = response.access_token

        val intent = Intent(this, DashboardActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun closeApp() {
        Toast.makeText(this, "Session Expired. Please re-login.", Toast.LENGTH_LONG).show()
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        if (prefManager.otp == null) {
            Handler().postDelayed({
                startActivity(Intent(this, LoginActivity::class.java))
            }, 5000)
        } else {
//            mPresenter.otpRegister(prefManager.phone!!, prefManager.otp!!.toInt())
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}
