package untag.f4_1461600225daskom.fitcare.ui.mitra_register

import okhttp3.MultipartBody
import okhttp3.RequestBody
import untag.f4_1461600225daskom.fitcare.model.RegisMitra
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface PendaftaranMitraUploadContract {
    interface View: BaseMvpView {
        fun showResponseMessage(response: RegisMitra.RegisMitraResponse)

        fun hideProgress()
        fun hideProgressMessage(it: Throwable?)
    }
    interface Presenter: BaseMvpPresenter<View> {
        fun subimitRegisMitra(token: String,
                              service_id: RequestBody,
                              full_name: RequestBody,
                              address: RequestBody,
                              phone: RequestBody,
                              email: RequestBody,
                              gender: RequestBody,
                              nik: RequestBody,
                              foto_selfie: MultipartBody.Part,
                              ktp: MultipartBody.Part,
                              skck: MultipartBody.Part,
                              certificate: MultipartBody.Part

        )

        fun subimitParticipantRegisMitra(token: String,
                             schedule_id: RequestBody,
                              full_name: RequestBody,
                              address: RequestBody,
                              phone: RequestBody,
                              email: RequestBody,
                              gender: RequestBody,
                              nik: RequestBody,
                              foto_selfie: MultipartBody.Part,
                              ktp: MultipartBody.Part

        )

        fun subimitVerif(token: String,
                                         foto_selfie: MultipartBody.Part,
                                         ktp: MultipartBody.Part

        )
    }
}