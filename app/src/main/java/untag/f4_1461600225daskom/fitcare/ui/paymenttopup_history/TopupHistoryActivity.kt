package untag.f4_1461600225daskom.fitcare.ui.paymenttopup_history

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.frogobox.recycler.boilerplate.viewrclass.FrogoViewAdapterCallback
import kotlinx.android.synthetic.main.activity_belum_sertifikasi.*
import kotlinx.android.synthetic.main.activity_term_jadi_mitra.*
import kotlinx.android.synthetic.main.activity_term_jadi_mitra.toolbar
import kotlinx.android.synthetic.main.activity_topup_item_history.view.*
import untag.f4_1461600225daskom.fitcare.R
import untag.f4_1461600225daskom.fitcare.model.BelumSertifikasi
import untag.f4_1461600225daskom.fitcare.model.TopUp
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpActivity
import untag.f4_1461600225daskom.fitcare.ui.belumsertifikasi.BelumSertifikasiDetailActivity
import untag.f4_1461600225daskom.fitcare.ui.paymenttopup.IsiSaldoContract
import untag.f4_1461600225daskom.fitcare.ui.paymenttopup.IsiSaldoPresenter
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager
import untag.f4_1461600225daskom.fitcare.utils.customFormat

class TopupHistoryActivity : BaseMvpActivity<TopupHistoryContract.View, TopupHistoryContract.Presenter>(), TopupHistoryContract.View {

    override var mPresenter: TopupHistoryContract.Presenter = TopupHIstoryPresenter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_topup_history)

        setToolbar()

        PreferenceManager(this).token?.let {
            mPresenter.getTopupHistory("Bearer $it")
        }
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun showResponse(response: TopUp.TopUpHistoryResponse) {
        val list: List<TopUp.Wallet_charge> = response.data.wallet.wallet_charge
        if (response.meta.code == 200){
            val adapterCallback = object :
                FrogoViewAdapterCallback<TopUp.Wallet_charge> {
                @SuppressLint("ResourceAsColor")
                override fun setupInitComponent(view: View, data: TopUp.Wallet_charge) {
                    view.tv_acc_bank.text = "${data.bank} - ${data.no_virtual}"
                    view.tv_history_date.text = data.created_at
                    view.tv_transaction_code.text = data.transaction_code
                    view.tv_history_state.text = data.status

                    if (data.transaction_code.toLowerCase().contains("adm")){
                        view.tv_acc_bank.visibility = GONE
                        view.v_line.setBackgroundResource(R.color.colorAccent)
                        view.tv_nominal.text = "Rp${data.nominal}"
                    }else{
                        view.tv_acc_bank.visibility = VISIBLE
                        view.v_line.setBackgroundResource(R.color.colorPrimary)
                        view.tv_nominal.text = "Rp${data.nominal.toInt()-4000}"
                    }
                }

                override fun onItemClicked(data:TopUp.Wallet_charge) {

                }

                override fun onItemLongClicked(data: TopUp.Wallet_charge) {
                    // setup item long clicked on frogo recycler view
                }
            }

            recycler_view
                .injector<TopUp.Wallet_charge>()
                .addData(list)
                .addCustomView(R.layout.activity_topup_item_history)
                .addEmptyView(null)
                .addCallback(adapterCallback)
                .createLayoutLinearVertical(false)
                .build()
        }
    }

    override fun hideProgress() {
    }
}