package untag.f4_1461600225daskom.fitcare.ui.otp

import untag.f4_1461600225daskom.fitcare.model.OtpResponse
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpPresenter
import untag.f4_1461600225daskom.fitcare.mvp.BaseMvpView

interface OtpContract {
    interface View:BaseMvpView{
        fun showResponseMessage(response: OtpResponse)
        fun hideProgress()

        fun showResendSuccessResponse()
        fun showResendFailResponse()

        fun showUpdatePhoneResponse()
    }

    interface Presenter:BaseMvpPresenter<View>{
        fun otpRegister(phone:String, otp:Int)

        fun submitPhoneNumber(phone: String)

        fun updateProfilePhone(token: String, phoneNew: String, otp: Int)

        fun resendProfileOtp(token: String, phoneNew: String)
    }
}