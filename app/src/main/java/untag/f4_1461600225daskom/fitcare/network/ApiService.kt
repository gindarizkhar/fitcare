package untag.f4_1461600225daskom.fitcare.network

import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import rx.Observable
import untag.f4_1461600225daskom.fitcare.model.*

interface ApiService {

    //LOGIN
    @POST("customer/login")
    @FormUrlEncoded
    fun login(
        @Field("phone") phone: String): Observable<Login.LoginResponse>

    //OJOK SAMPEK TABLEMU KUWALEK, LEK KUWALEK NGECHECK E NAK API SERVICE IKI
    //REGISTER
    @POST("customer/register")
    @FormUrlEncoded
    fun register(
        @Field("phone") phone: String,
        @Field("full_name") fullname:String,
        @Field("email") email: String): Observable<Register.RegisterResponse>

    /*@POST("register")
    @FormUrlEncoded
    fun register(
        @Field("phone") phone: String,
        @Field("email") email: String,
        @Field("full_name") fullname: String): Observable<register.resgisterrespon>*/

    //REGISTER OTP
    @POST("customer/register/confirm")
    @FormUrlEncoded
    fun registerOtp(
        @Field("phone") phone: String,
        @Field("otp") otp: Int): Observable<OtpResponse>

    //GET ALL HOME
    @GET("customer/home")
    fun getAll(@Header("Authorization")token: String): Observable<Home.HomeResponse>

    //GET PROFILE DATA
    @GET("customer/profile/data")
    fun getProfile(@Header("Authorization")token: String):Observable<Profile.ProfileResponse>

    @PUT("customer/profile/name")
    @FormUrlEncoded
    fun updateProfileName(@Header("Authorization")token: String,
                          @Field("full_name") fullName: String): Observable<Profile.UpdateNameResponse>

    @PUT("customer/profile/address")
    @FormUrlEncoded
    fun updateProfileAddress(@Header("Authorization")token: String,
                             @Field("address")address: String): Observable<Profile.UpdateAddressResponse>

    @POST("customer/profile/phone/edit")
    @FormUrlEncoded
    fun editProfilePhone(@Header("Authorization")token: String,
                         @Field("phone_new") phoneNew: String): Observable<Profile.UpdatePhoneResponse>

    @PUT("customer/profile/phone/update")
    @FormUrlEncoded
    fun verifyProfilePhone(@Header("Authorization")token: String,
                           @Field("phone_new") phoneNew: String,
                           @Field("otp") otp: Int): Observable<Profile.VerifyPhoneResponse>

    @PUT("customer/profile/email/edit")
    @FormUrlEncoded
    fun editProfileEmail(@Header("Authorization")token: String,
                         @Field("email") email: String): Observable<Profile.UpdateEmailResponse>

    @POST("customer/profile/email/verify")
    fun verifyEmail(@Header("Authorization")token: String): Observable<Profile.VerifyEmailResponse>

    @POST("customer/profile/image")
    @Multipart
    fun uploadProfileImage(@Header("Authorization")token: String,
                           @Part image: MultipartBody.Part): Observable<Profile.UploadProfileImageResponse>

    @GET("customer/order")
    fun getAllHistory(@Header("Authorization")token: String): Observable<History.HistoryResponse>

    @GET("customer/order/detail/{id}")
    fun getDetailHistory(@Header("Authorization")token: String,@Path("id")id: String): Observable<HistoryDetail.HistoryResponse>

    @POST("customer/order/store")
    @FormUrlEncoded
    fun submitOrder(@Header("Authorization")token: String,
                    @Field("service_id")serviceId: String,
                    @Field("longitude")longitude: String,
                    @Field("latitude")latitude: String,
                    @Field("time")time: String,
                    @Field("note")note: String,
                    @Field("duration")duration: String,
                    @Field("payment")payment: String): Observable<Order.OrderResponse>

    @POST("customer/register/mitra")
    @Multipart
    fun subimitRegisMitra(@Header("Authorization")token: String,
                    @Part("service_id")service_id: RequestBody,
                    @Part("full_name")full_name: RequestBody,
                    @Part("address")address: RequestBody,
                    @Part("phone")phone: RequestBody,
                    @Part("email")email: RequestBody,
                    @Part("gender")gender: RequestBody,
                    @Part("nik")nik: RequestBody,
                    @Part foto_selfie: MultipartBody.Part,
                    @Part ktp: MultipartBody.Part,
                    @Part skck: MultipartBody.Part,
                    @Part certificate: MultipartBody.Part
    ): Observable<RegisMitra.RegisMitraResponse>

    @POST("customer/register/participant")
    @Multipart
    fun subimitParticipantRegisMitra(@Header("Authorization")token: String,
                          @Part("schedule_id")schedule_id: RequestBody,
                          @Part("full_name")full_name: RequestBody,
                          @Part("address")address: RequestBody,
                          @Part("phone")phone: RequestBody,
                          @Part("email")email: RequestBody,
                          @Part("gender")gender: RequestBody,
                          @Part("nik")nik: RequestBody,
                          @Part foto_selfie: MultipartBody.Part,
                          @Part ktp: MultipartBody.Part
    ): Observable<RegisMitra.RegisMitraResponse>

    @POST("customer/validation/saldo")
    @Multipart
    fun subimitVerif(@Header("Authorization")token: String,

                                     @Part foto_selfie: MultipartBody.Part,
                                     @Part ktp: MultipartBody.Part
    ): Observable<RegisMitra.RegisMitraResponse>

    @PUT("customer/update/token_fcm")
    @FormUrlEncoded
    fun submitFCM(@Header("Authorization")authorization: String,
                    @Field("token")token: String
                   ): Observable<Dashboard.DashboardResponse>

    @PUT("customer/order/update/order_success/{id}")
    fun submitSelesai(@Header("Authorization")authorization: String,
                      @Path("id")id: String
    ): Observable<HistoryDetail.HistoryResponse>

    @GET("customer/schedule")
    fun getSertifikasi(@Header("Authorization")authorization: String
    ): Observable<BelumSertifikasi.BelumSertifikasiResponse>

    @GET("customer/schedule/detail/{id}")
    fun getSertifikasiDetail(@Header("Authorization")authorization: String,
                             @Path("id")id: String
    ): Observable<BelumSertifikasi.BelumSertifikasiDetailResponse>

    @POST("midtrans/top_up/before_transactions")
    @FormUrlEncoded
    fun insertMidtrans(@Header("Authorization")authorization: String,
                             @Field("nominal")nominal: String,
                             @Field("bank")bank: String
    ): Observable<TopUp.TopUpResponse>

    @GET("midtrans/top_up/history")
    fun getTopupHistory(@Header("Authorization")authorization: String
    ): Observable<TopUp.TopUpHistoryResponse>
}