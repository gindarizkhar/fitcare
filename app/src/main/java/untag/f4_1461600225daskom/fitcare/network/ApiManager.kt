package untag.f4_1461600225daskom.fitcare.network

import okhttp3.Interceptor
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import untag.f4_1461600225daskom.fitcare.BuildConfig
import java.util.concurrent.TimeUnit

object ApiManager{
    private const val SERVER: String = BuildConfig.BASE_URL

    private lateinit var mApiService: ApiService

    init {
        val retrofit = initRetrofit()
        initServices(retrofit)
    }

    private fun initRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        val client = OkHttpClient.Builder().apply {
            networkInterceptors().add(Interceptor { chain ->
                val original = chain.request()
                val request = original.newBuilder()
                    .method(original.method, original.body)
                    .build()
                chain.proceed(request)
            })
            addInterceptor(interceptor)
        }



        return Retrofit.Builder().baseUrl(SERVER)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create().asLenient().withNullSerialization())
            .client(client.connectTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS).build())
            .build()
    }

    private fun initServices(retrofit: Retrofit) {
        mApiService = retrofit.create(ApiService::class.java)
    }

    fun login(phone: String)=
        mApiService.login(phone)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun register(phone: String, email: String, fullname: String)=
        mApiService.register(phone, email, fullname)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun otp(phone: String, otp: Int)=
        mApiService.registerOtp(phone, otp)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun home(token:String)=
        mApiService.getAll(token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun getProfile(token: String)=
        mApiService.getProfile(token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun updateProfileName(token: String, fullname: String)=
        mApiService.updateProfileName(token, fullname)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun updateProfileAddress(token: String, address: String) =
        mApiService.updateProfileAddress(token, address)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun editProfilePhone(token: String, phoneNew: String) =
        mApiService.editProfilePhone(token, phoneNew)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun verifyProfilePhone(token: String, phoneNew: String, otp: Int) =
        mApiService.verifyProfilePhone(token, phoneNew, otp)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun editProfileEmail(token: String, email: String) =
        mApiService.editProfileEmail(token, email)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun verifyEmail(token: String) =
        mApiService.verifyEmail(token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun uploadProfileImage(token: String, image: MultipartBody.Part) =
        mApiService.uploadProfileImage(token, image)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun getAllHistory(token: String) =
        mApiService.getAllHistory(token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun getDetailHistory(token: String,id:String) =
        mApiService.getDetailHistory(token,id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun submitOrder(token: String,
                    serviceId: String,
                    longitude: String,
                    latitude: String,
                    time: String,
                    note: String,
                    duration: String,
                    payment: String) =
        mApiService.submitOrder(token, serviceId, longitude, latitude, time, note, duration, payment)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun subimitRegisMitra(token: String,
                          service_id: RequestBody,
                          full_name: RequestBody,
                          address: RequestBody,
                          phone: RequestBody,
                          email: RequestBody,
                          gender: RequestBody,
                          nik: RequestBody,
                          foto_selfie: MultipartBody.Part,
                          ktp: MultipartBody.Part,
                          skck: MultipartBody.Part,
                          certificate: MultipartBody.Part

        ) =
        mApiService.subimitRegisMitra(token, service_id, full_name, address, phone, email, gender, nik, foto_selfie, ktp, skck, certificate)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun subimitParticipantRegisMitra(
        token: String,
        schedule_id: RequestBody,
        full_name: RequestBody,
        address: RequestBody,
        phone: RequestBody,
        email: RequestBody,
        gender: RequestBody,
        nik: RequestBody,
        foto_selfie: MultipartBody.Part,
        ktp: MultipartBody.Part

    ) =
        mApiService.subimitParticipantRegisMitra(token, schedule_id, full_name, address, phone, email, gender, nik, foto_selfie, ktp)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun subimitVerif(
        token: String,

        foto_selfie: MultipartBody.Part,
        ktp: MultipartBody.Part

    ) =
        mApiService.subimitVerif(token, foto_selfie, ktp)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun submitFCM(authorization: String,
                  token: String
                   ) =
        mApiService.submitFCM(authorization, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun submitSelesai(authorization: String,
                  id: String
                   ) =
        mApiService.submitSelesai(authorization, id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun getSertifikasi(authorization: String
    ) =
        mApiService.getSertifikasi(authorization)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun getSertifikasiDetail(authorization: String, id: String
    ) =
        mApiService.getSertifikasiDetail(authorization,id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun insertMidtrans(authorization: String, nominal: String, bank: String
    ) =
        mApiService.insertMidtrans(authorization, nominal, bank)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!

    fun getTopupHistory(authorization: String
    ) =
        mApiService.getTopupHistory(authorization)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())!!
}