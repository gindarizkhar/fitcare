package untag.f4_1461600225daskom.fitcare.utils

import android.content.Context

class PreferenceManager(context:Context) {
    companion object{
        private const val SHARED_PREFERENCE_NAME = "member_data"
        private const val TOKEN="token"
        private const val OTP="otp"
        private const val PHONE="phone"
    }
    private val sharedPreference = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE)
    private val editPreference = sharedPreference.edit()

    var token: String?
        get() = sharedPreference.getString(TOKEN,null)
        set(value) = editPreference.putString(TOKEN,value).apply()

    var otp: String?
        get() = sharedPreference.getString(OTP, null)
        set(value) = editPreference.putString(OTP, value).apply()

    var phone: String?
        get() = sharedPreference.getString(PHONE, null)
        set(value) = editPreference.putString(PHONE, value).apply()

    fun clearData() {
        editPreference.putString(TOKEN,null).apply()
        editPreference.putString(OTP,null).apply()
        editPreference.putString(PHONE,null).apply()
    }
}