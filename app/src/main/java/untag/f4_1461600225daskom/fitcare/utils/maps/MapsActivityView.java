package untag.f4_1461600225daskom.fitcare.utils.maps;

import android.location.Location;

import com.google.android.gms.tasks.Task;

public interface MapsActivityView {

    void onGetMyPosition(Task<Location> task);
}
