package untag.f4_1461600225daskom.fitcare.utils.maps;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import untag.f4_1461600225daskom.fitcare.R;
import untag.f4_1461600225daskom.fitcare.databinding.ActivityMapsBinding;
import untag.f4_1461600225daskom.fitcare.ui.orderconfirmation.OrderConfirmationActivity;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, MapsActivityView {

    private GoogleMap mMap;
    private LatLng mLatLng, destinationLatLng;
    private Location mLocation;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private String lat = "12", lng = "112";
    private MarkerOptions myLocation;

    Geocoder geocoder;
    List<Address> addresses;

    private ActivityMapsBinding binding;
    private MapsActivityPresenter presenter;
    private String address;
    private SupportMapFragment mapFragment;
    private static final int PLAY_SERVICES_ERROR_CODE = 9002;
    public static final int GPS_REQUEST_CODE = 9003;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        setTitle("Pilih Lokasi");
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        presenter = new MapsActivityPresenter(this, this);

        InitMap();

        binding.cvPilihAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsActivity.this, OrderConfirmationActivity.class);
                intent.putExtra("menit", getIntent().getStringExtra("menit"));
                intent.putExtra("price", getIntent().getStringExtra("price"));
                intent.putExtra("service_id", getIntent().getIntExtra("service_id", 1));
                intent.putExtra("latitude", lat);
                intent.putExtra("longitude", lng);
                intent.putExtra("address", address);
                startActivity(intent);
            }
        });
    }

    private void InitMap() {
        if (isServicesOk()) {
            if (isGPSEnabled()) {
                if (checkLocationPermission()) {
                    mLatLng = new LatLng(Double.parseDouble(lat),Double.parseDouble(lng));

                    mapFragment = (SupportMapFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.fragment_maps);

                    mapFragment.getMapAsync(this);

                    //untuk mendapatkan lokasi terkini
                    mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
                    geocoder = new Geocoder(this, Locale.getDefault());

                    binding.ibSearch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMap.clear();
                            List<Address> addressList = null;
                            if (binding.etAlamat.getText().toString().equals("")){
                                binding.etAlamat.setError("Lokasi harus diisi");
                            }else{
                                Context context;
                                Geocoder geocoder = new Geocoder(MapsActivity.this);
                                try {
                                    addressList = geocoder.getFromLocationName(binding.etAlamat.getText().toString(),1);
                                    Address address2 = addressList.get(0);
                                    LatLng latLng = new LatLng(address2.getLatitude(), address2.getLongitude());
                                    mMap.addMarker(new MarkerOptions().position(latLng).title(binding.etAlamat.getText().toString()));
                                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,16));

                                    lat = address2.getLatitude()+"";
                                    lng = address2.getLongitude()+"";
                                    address = binding.etAlamat.getText().toString();
                                }catch (IOException e){
                                    e.printStackTrace();
                                    Toast.makeText(MapsActivity.this, "Lokasi tidak ditemukan", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                    });
                } else {
                    presenter.checkGpsPermissions(mMap);
                }
            }
        }
    }

    private boolean checkLocationPermission() {

        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    private boolean isGPSEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        boolean providerEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (providerEnabled) {
            return true;
        } else {

            new AlertDialog.Builder(this)
                    .setTitle("GPS Permissions")
                    .setMessage("GPS is required for this app to work. Please enable GPS.")
                    .setPositiveButton("Yes", ((dialogInterface, i) -> {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(intent, GPS_REQUEST_CODE);
                        finish();
                    }))
                    .setCancelable(false)
                    .show();

        }

        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        try {
            mMap.setMyLocationEnabled(true);
            presenter.getMyPosition(mFusedLocationProviderClient);

            binding.fabLokasi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.getMyPosition(mFusedLocationProviderClient);
                }
            });
        } catch (SecurityException e) {
            presenter.checkGpsPermissions(mMap);
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    public void onGetMyPosition(Task<Location> task) {
        mMap.clear();
        mLocation = task.getResult();
        if (mLocation != null) {
            mLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());

            lat = mLocation.getLatitude() + "";
            lng = mLocation.getLongitude() + "";
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng,
                16
        ));
        myLocation = new MarkerOptions().position(mLatLng).title("my position");

        try {
            addresses = geocoder.getFromLocation(mLocation.getLatitude(), mLocation.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

            binding.etAlamat.setText(address);
        } catch (Exception e) {
//            e.printStackTrace();
            presenter.getMyPosition(mFusedLocationProviderClient);
        }



    }

    private boolean isServicesOk() {

        GoogleApiAvailability googleApi = GoogleApiAvailability.getInstance();

        int result = googleApi.isGooglePlayServicesAvailable(this);

        if (result == ConnectionResult.SUCCESS) {
            return true;
        } else if (googleApi.isUserResolvableError(result)) {
            Dialog dialog = googleApi.getErrorDialog(this, result, PLAY_SERVICES_ERROR_CODE, task ->
                    Toast.makeText(this, "Dialog is cancelled by User", Toast.LENGTH_SHORT).show());
            dialog.show();
        } else {
            Toast.makeText(this, "Play services are required by this application", Toast.LENGTH_SHORT).show();
        }
        return false;
    }
}
