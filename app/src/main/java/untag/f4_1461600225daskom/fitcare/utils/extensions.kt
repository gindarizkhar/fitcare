package untag.f4_1461600225daskom.fitcare.utils

import com.squareup.moshi.Moshi
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

val localeId = Locale("in", "ID")

fun String.formatDate(pattern: String): String {
    val initialFormat = SimpleDateFormat(pattern, Locale.ENGLISH)
    val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
    val date = initialFormat.parse(this)
    return format.format(date).toString()
}

fun String.customFormat(patternInit: String, pattern: String): String {
    val initialFormat = SimpleDateFormat(patternInit, Locale.ENGLISH)
    val formatOutput = SimpleDateFormat(pattern, localeId)
    val date = initialFormat.parse(this)
    return formatOutput.format(date).toString()
}

fun Float.formatCurrency(): String{
    val formatter = NumberFormat.getCurrencyInstance(localeId)
    return formatter.format(this).replace("Rp", "")
}

inline fun <reified T> Moshi.fromJson(json: String): T = this.adapter(T::class.java).fromJson(json)!!