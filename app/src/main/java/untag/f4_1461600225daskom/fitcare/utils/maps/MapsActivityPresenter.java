package untag.f4_1461600225daskom.fitcare.utils.maps;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class MapsActivityPresenter {
    private MapsActivityView view;
    private Context context;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private GoogleMap googleMap;

    public MapsActivityPresenter(MapsActivityView view, Context context) {
        this.view = view;
        this.context = context;
    }

    public void getMyPosition(FusedLocationProviderClient mFusedLocationProviderClient) {
        try{
            fusedLocationProviderClient = mFusedLocationProviderClient;
            Task<Location> locationTask = mFusedLocationProviderClient.getLastLocation();
            locationTask.addOnCompleteListener((Activity) context, new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if(task.isSuccessful()){
                        view.onGetMyPosition(task);
                    }else{
                        Toast.makeText(context, "Belum Ada Akses GPS", Toast.LENGTH_SHORT).show();
                        checkGpsPermissions(googleMap);
                    }
                }
            });
        }catch (SecurityException e){
            Log.e("Exception: %s", e.getMessage());
        }
    }

    public void checkGpsPermissions(GoogleMap mMap) {
        googleMap = mMap;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, "Akses GPS Aktif", Toast.LENGTH_SHORT).show();
            mMap.setMyLocationEnabled(true);
            getMyPosition(fusedLocationProviderClient);
        }else{
            ActivityCompat.requestPermissions((Activity) context,new String[] { Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }
}
