package untag.f4_1461600225daskom.fitcare.utils.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import untag.f4_1461600225daskom.fitcare.R;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    private Bitmap bitmap;

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
//        Log.d(TAG,"NEW_TOKEN: "+token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "FROM:" + remoteMessage.getMessageType());


        //Check if the message contains data
        if(remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data: " + remoteMessage.getData());

        }


        //Check if the message contains notification

        if(remoteMessage.getNotification() != null && remoteMessage.getData().get("foto") != null) {
            Log.d(TAG, "Mesage foto:" + remoteMessage.getData().get("foto"));
            bitmap = getBitmapfromUrl(remoteMessage.getData().get("foto"));
            sendNotification(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(), bitmap);
        }else if (remoteMessage.getNotification() != null){
            Log.d(TAG, "Mesage body:" + remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(), null);
        }

        if (remoteMessage.getNotification().getBody().contains("Selamat Ulang Tahun")){
            PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("birthday", true).apply();
            Log.d("cek birthday", PreferenceManager.getDefaultSharedPreferences(this).getBoolean("birthday", false)+"");
        }
    }

    /**
     * Dispay the notification
     * @param body
     */
    private void sendNotification(String title, String body, Bitmap foto) {
        String idChannel = "my_channel_01";
        NotificationChannel mChannel = null;
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (foto != null) {

            String CHANNEL_ID = "Channel_1";
            String CHANNEL_NAME = "AlarmManager channel";

            NotificationManager notificationManagerCompat = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setStyle(new NotificationCompat.BigPictureStyle()
                                .bigPicture(foto))/*Notification with Image*/
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setSound(alarmSound);

        /*
        Untuk android Oreo ke atas perlu menambahkan notification channel
        Materi ini akan dibahas lebih lanjut di modul extended
         */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                /* Create or update. */
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                        CHANNEL_NAME,
                        NotificationManager.IMPORTANCE_DEFAULT);

                channel.enableVibration(true);
                channel.setVibrationPattern(new long[]{1000, 1000, 1000, 1000, 1000});

                builder.setChannelId(CHANNEL_ID);

                if (notificationManagerCompat != null) {
                    notificationManagerCompat.createNotificationChannel(channel);
                }
            }

            Notification notification = builder.build();

            if (notificationManagerCompat != null) {
                notificationManagerCompat.notify(0, notification);
            }
        }else {

            String CHANNEL_ID = "Channel_1";
            String CHANNEL_NAME = "AlarmManager channel";

            NotificationManager notificationManagerCompat = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setSound(alarmSound);

        /*
        Untuk android Oreo ke atas perlu menambahkan notification channel
        Materi ini akan dibahas lebih lanjut di modul extended
         */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                /* Create or update. */
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                        CHANNEL_NAME,
                        NotificationManager.IMPORTANCE_DEFAULT);

                channel.enableVibration(true);
                channel.setVibrationPattern(new long[]{1000, 1000, 1000, 1000, 1000});

                builder.setChannelId(CHANNEL_ID);

                if (notificationManagerCompat != null) {
                    notificationManagerCompat.createNotificationChannel(channel);
                }
            }

            Notification notification = builder.build();

            if (notificationManagerCompat != null) {
                notificationManagerCompat.notify(0, notification);
            }
        }

        if (body.contains("dikirim") || body.contains("Resi telah diupdate")){
            Log.d("notifreload","yes");
            Intent intent = new Intent("reloadDetailTransaksi");
            //            intent.putExtra("quantity",Integer.parseInt(quantity.getText().toString()));
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }
        
    }




    /*
     *To get a Bitmap image from the URL received
     * */
    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }
}
