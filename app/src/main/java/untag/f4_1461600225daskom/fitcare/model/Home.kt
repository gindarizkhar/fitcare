package untag.f4_1461600225daskom.fitcare.model

object Home {
    data class HomeResponse(val meta: Meta, val data: Data)
    data class Data(val user: User, val services: MutableList<Service>, val news: MutableList<News>, val wallet: Wallet)
    data class User(
        val id:Int,
        val username:String,
        val email:String,
        val phone:String,
        val role:Int,
        val user_data: UserData,
        val wallet: Wallet
    )

    data class Service(
        val id: Int,
        val service: String,
        val description: String,
        val icon: String
    )

    data class News(
        val content: String,
        val created_at: String,
        val id: Int,
        val image: String,
        val link_news: String,
        val status: String,
        val title: String,
        val updated_at: String
    )

    data class UserData(
        val id: Int,
        val user_id: Int,
        val full_name: String,
        val gender: String,
        val address: String
    )
    data class Wallet (
        var id: Int,
        var user_id: String,
        var amount: String,
        var status: String,
        var created_at: String,
        var updated_at: String
    )
}