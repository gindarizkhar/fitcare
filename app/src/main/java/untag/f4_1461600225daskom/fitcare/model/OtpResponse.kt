package untag.f4_1461600225daskom.fitcare.model

data class OtpResponse(
    val access_token: String,
    val token_type: String,
    val expires_in: Int?
)