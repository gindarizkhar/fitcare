package untag.f4_1461600225daskom.fitcare.model


object BelumSertifikasi {
    data class BelumSertifikasiResponse(val meta: Meta, val data: Data)
    data class BelumSertifikasiDetailResponse(val meta: Meta, val data: DataDetail)

    data class Data (var schedule: List<Schedule>)
    data class DataDetail (var schedule: Schedule)

    data class Schedule (
        var id: Int,
        var service_id: String,
        var price: String,
        var schedule_code: String,
        var date_regist: String,
        var date_regist_expired: String,
        var date_certificate: String,
        var date_certificate_expired: String,
        var status: Int,
        var participant_max: String,
        var participant_regist: String,
        var content: String,
        var created_at: String,
        var updated_at: String,
        var service: Service
    )

    data class Service (
        var id: Int,
        var service: String,
        var description: String,
        var icon: String,
        var status: String,
        var created_at: String,
        var updated_at: String
    )
}