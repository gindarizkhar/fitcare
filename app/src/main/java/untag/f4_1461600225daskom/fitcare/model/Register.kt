package untag.f4_1461600225daskom.fitcare.model

object Register {
    data class RegisterResponse(val meta: Meta, val data: Data)
    data class Data(val user: User, val user_data: User_Data, val otp: Int)
    data class User(
        val phone: String,
        val email: String,
        val role: Int,
        val id: Int
    )

    data class User_Data(
        val user_id: Int,
        val full_name: String,
        val id: Int
    )
}