package untag.f4_1461600225daskom.fitcare.model


object TopUp {
    data class TopUpResponse(val meta: Meta, val data: Data)

    data class TopUpHistoryResponse(val meta: Meta, val data: DataHistory)

    data class Data (
        var wallet: Wallet,
        var wallet_charge: Wallet_charge
    )

    data class DataHistory (
        var wallet: Wallet
    )

    data class Wallet (
        var id: Int,
        var user_id: String,
        var amount: String,
        var status: String,
        var created_at: String,
        var updated_at: String,
        var wallet_charge: List<Wallet_charge>
    )
    
    data class Wallet_charge (
        var wallet_id: Int,
        var transaction_code: String,
        var nominal: String,
        var bank: String,
        var receiver: String,
        var no_virtual: String,
        var status: String,
        var updated_at: String,
        var created_at: String,
        var id: Int
    )
}