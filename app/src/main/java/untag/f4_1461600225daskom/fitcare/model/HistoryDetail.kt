package untag.f4_1461600225daskom.fitcare.model


object HistoryDetail {
    data class HistoryResponse(val meta: Meta, val data: DetailOrderData)

    data class DetailOrderData(val order: HistoryDetail.Order)

    data class Order(
        val created_at: String,
        val duration: String,
        val id: Int,
        val invoice: Invoice,
        val latitude: String,
        val longitude: String,
        val note: String,
        val payment: String,
        val service_id: String,
        val time: String,
        val updated_at: String,
        val user_id: String
    )

    data class Invoice(
        val created_at: String,
        val details: Details,
        val id: Int,
        val invoice_code: String,
        val mitra_id: String,
        val order_id: String,
        val status: String,
        val updated_at: String,
        val mitra_details: Mitra_details
    )

    data class Details(
        val distance: String,
        val duration: Int,
        val loc_mitra_now: String,
        val price: Int,
        val time_trip: String,
        val total: Int,
        val transport: Int
    )

    data class Mitra_details (
        var id: Int,
        var user_role_id: String,
        var username: String,
        var email: String,
        var phone: String,
        var created_at: String,
        var updated_at: String,
        var user_data: User_data
    )

    data class User_data (
        var id: Int,
        var user_id: String,
        var full_name: String,
        var birthday: Any,
        var gender: String,
        var address: String,
        var display_picture: Any,
        var created_at: String,
        var updated_at: String
    )
}