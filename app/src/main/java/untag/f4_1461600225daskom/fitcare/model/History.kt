package untag.f4_1461600225daskom.fitcare.model

object History {

    data class HistoryResponse(val meta: Meta, val data: HistoryData)

    data class DetailOrderResponse(val meta: Meta, val data: DetailOrderData)

    data class HistoryData(val order: List<Order>)

    data class DetailOrderData(val order: Order)

    data class Order(
        val created_at: String,
        val duration: String,
        val id: Int,
        val invoice: Invoice,
        val latitude: String,
        val longitude: String,
        val note: String,
        val payment: String,
        val service_id: String,
        val time: String,
        val updated_at: String,
        val user_id: String
    )

    data class Invoice(
        val created_at: String,
        val details: Details,
        val id: Int,
        val invoice_code: String,
        val mitra_id: String,
        val order_id: String,
        val status: String,
        val updated_at: String
    )

    data class Details(
        val distance: String,
        val duration: Int,
        val loc_mitra_now: String,
        val price: Int,
        val time_trip: String,
        val total: Int,
        val transport: Int
    )
}