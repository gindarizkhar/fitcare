package untag.f4_1461600225daskom.fitcare.model

object Login {
    data class LoginResponse(val meta: LoginMeta)

    data class LoginMeta(
        val code: Int,
        val message: String,
        val OTP: Int
    )
}