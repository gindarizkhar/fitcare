package untag.f4_1461600225daskom.fitcare.model


object Order {
    data class OrderResponse(val meta: Meta, val data: OrderData)
    
    data class OrderData(val invoice: Invoice, val mitra: Mitra, val order: Order2,val customer: Customer )

    data class Invoice (
        var order_id: Int,
        var mitra_id: String,
        var invoice_code: String,
        var details: DetailInvoice,
        var updated_at: String,
        var created_at: String,
        var id: Int
    )

    data class DetailInvoice(
        val duration: String,
        val price: String,
        val total: Int,
        val transport: Int
    )

    data class Order2(
        val created_at: String,
        val duration: String,
        val id: Int,
        val latitude: String,
        val longitude: String,
        val note: String,
        val payment: String,
        val service_id: String,
        val time: String,
        val updated_at: String,
        val user_id: String
    )

    data class Mitra(
        val created_at: String,
        val email: String,
        val id: Int,
        val phone: String,
        val updated_at: String,
        val user_data: UserData,
        val user_role_id: String,
        val username: String
    )

    data class Customer(
        val created_at: String,
        val email: String,
        val id: Int,
        val phone: String,
        val updated_at: String,
        val user_data: UserData,
        val user_role_id: String,
        val username: String
    )

    data class UserData(
        val address: String,
        val birthday: String,
        val created_at: String,
        val display_picture: String?,
        val full_name: String,
        val gender: String,
        val id: Int,
        val updated_at: String,
        val user_id: String
    )
}