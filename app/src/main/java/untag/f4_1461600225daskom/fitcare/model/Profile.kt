package untag.f4_1461600225daskom.fitcare.model

object Profile {

    data class ProfileResponse(
        val data: Data,
        val meta: Meta
    )

    data class Data(
        val user: User
    )

    data class User(
        val created_at: String,
        val email: String,
        val id: Int,
        val phone: String,
        val updated_at: String,
        val user_data: UserData,
        val user_role_id: String,
        val user_verification_status: UserVerificationStatus,
        val username: String
    )

    data class UserData(
        val address: String,
        val birthday: String,
        val created_at: String,
        val display_picture: String,
        val full_name: String,
        val gender: String,
        val id: Int,
        val updated_at: String,
        val user_id: String
    )

    data class UserVerificationStatus(
        val created_at: String,
        val id: Int,
        val status: String,
        val updated_at: String,
        val user_id: String
    )

    data class UpdatePhoneResponse(
        val data: UpdatePhoneData,
        val meta: Meta
    )

    data class UpdatePhoneData(
        val OTP: Int,
        val phone_new: String,
        val phone_old: String,
        val user_id: Int
    )

    data class UpdateNameResponse(val meta: Meta, val data: UpdateNameData)

    data class UpdateNameData(val user_data: UpdateNameUserData)

    data class UpdateNameUserData(
        val full_name: String,
        val id: Int,
        val user_id: String
    )

    data class UpdateAddressResponse(val meta: Meta, val data: UpdateAddressData)

    data class UpdateAddressData(val user_data: UpdateAddressUserData)

    data class UpdateAddressUserData(
        val address: String,
        val id: Int,
        val user_id: String
    )

    data class VerifyPhoneResponse(
        val id:Int,
        val username:String,
        val email:String,
        val phone:String,
        val role:Int,
        val created_at: String,
        val updated_at: String,
        val otp: Otp
    )

    data class Otp(
        val id: Int,
        val user_id: String,
        val otp: Int,
        val expired_at: String,
        val created_at: String,
        val updated_at: String
    )

    data class UpdateEmailResponse(val meta: Meta, val data: UpdateEmailData)

    data class VerifyEmailResponse(val meta: Meta)

    data class UpdateEmailData(
        val email: String,
        val user_id: Int,
        val user_verification_status: Int
    )

    data class UploadProfileImageResponse(
        val `data`: UploadImageData,
        val meta: Meta
    )

    data class UploadImageData(
        val user_data: UploadImageUserData
    )

    data class UploadImageUserData(
        val display_picture: String,
        val id: Int,
        val user_id: String
    )

}