package untag.f4_1461600225daskom.fitcare.mvp

import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import android.widget.Toast
import untag.f4_1461600225daskom.fitcare.ui.login.LoginActivity
import untag.f4_1461600225daskom.fitcare.utils.PreferenceManager

/**
 * Created by andrewkhristyan on 10/2/16.
 */
abstract class BaseMvpFragment<in V : BaseMvpView, T : BaseMvpPresenter<V>>
    : androidx.fragment.app.Fragment(), BaseMvpView {


    var ctx: Context? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mPresenter.attachView(this as V)
        ctx=context
    }
    override fun getContext(): Context{
        return ctx!!
    }

    protected abstract var mPresenter: T

    override fun showError(error: String?) {
        Toast.makeText(ctx, error, Toast.LENGTH_LONG).show()
    }

    override fun showError(stringResId: Int) {
        Toast.makeText(ctx, stringResId, Toast.LENGTH_LONG).show()
    }

    override fun showMessage(srtResId: Int) {
        Toast.makeText(ctx, srtResId, Toast.LENGTH_LONG).show()
    }

    override fun showMessage(message: String) {
        Toast.makeText(ctx, message, Toast.LENGTH_LONG).show()
        if (message.contains("Unauthorized")){
            ctx?.let { PreferenceManager(it).clearData() }
            val logout = Intent(ctx, LoginActivity::class.java)
            startActivity(logout)
        }
    }

    override fun onDetach() {
        super.onDetach()
        mPresenter.detachView()
        ctx=null
    }
}